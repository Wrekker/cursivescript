using System.Numerics;
using System.Runtime.Remoting.Messaging;
using System.Security.Authentication.ExtendedProtection;
using CursiveScript.Analysis;
using CursiveScript.CTypes;
using CursiveScript.Primitives;
using CursiveScript.Syntax.Nodes;

namespace CursiveScript.Transformation
{
    public class BasicRewriter : RewriterBase
    {
        public BasicRewriter() { }


        public ICType RewriteTree(ICType ast) {
            return base.Rewrite(ast);
        }

        protected override ICType RewriteNull() {
            return Pair.Null;
        }

        protected override ICType RewriteConstantBase(ICType expression) {
            return expression;
        }

        protected override Symbol RewriteSymbolBase(Symbol sym) {
            //ICType res;
            //if (Environment.Lookup(sym) != CType.None) {
            //    res = RewriteVariable(new VariableSyntaxNode(sym));
            //} else {
            //    res = RewriteSymbol(sym);
            //}
            //return (Symbol)res;
            return sym;
        }

        protected override ICType RewriteListBase(Pair list)
        {
            //ICType res;
            //if (SequenceSyntaxNode.Is(list)) {
            //    res = RewriteSequence(new SequenceSyntaxNode(list));
            //} else if (AssignmentSyntaxNode.Is(list)) {
            //    res = RewriteAssignment(new AssignmentSyntaxNode(list));
            //} else if (DefinitionSyntaxNode.Is(list)) {
            //    res = RewriteDefinition(new DefinitionSyntaxNode(list));
            //} else if (LambdaSyntaxNode.Is(list)) {
            //    res = RewriteLambda(new LambdaSyntaxNode(list));
            //} else if (ConditionSyntaxNode.Is(list)) {
            //    res = RewriteCondition(new ConditionSyntaxNode(list));
            //} else if (LiteralSyntaxNode.Is(list)) {
            //    res = RewriteLiteral(new LiteralSyntaxNode(list));
            //} else if (WhileSyntaxNode.Is(list)) {
            //    res = RewriteWhile(new WhileSyntaxNode(list));
            //} else {
            //    //ICType car = list.Car;
            //    //if (car.IsType(CType.Symbol) && Environment.Lookup((Symbol) car).HasFlag(CType.Procedure) ||
            //    //    car.IsType(CType.Procedure)) {
            //    res = RewriteApplication(new ApplicationSyntaxNode(list));
            //    //} else {
            //    //    res = RewriteList(list);
            //    //}
            //}

            //return res;
            return list;
        }

        //private ICType RewriteSymbol(Symbol sym) {
        //    return sym;
        //}

        //protected virtual ICType RewriteList(Pair list) {
        //    return list.MapTree(Rewrite);
        //}


        //protected virtual ICType RewriteCondition(ConditionSyntaxNode syntaxNode) {
        //    return SyntaxNode.CreateCondition(Rewrite(syntaxNode.Predicate),
        //                              Rewrite(syntaxNode.TrueBranch),
        //                              Rewrite(syntaxNode.FalseBranch));
        //}

        ////protected virtual ICType RewriteMacro(MacroSyntaxNode syntaxNode) {
        ////    //Environment.Define(syntaxNode.Variable, CType.Macro);
        ////    return SyntaxNode.CreateMacro(
        ////        Rewrite(syntaxNode.Variable),
        ////        Rewrite(syntaxNode.Parameters),
        ////        Rewrite(syntaxNode.Body));
        ////}
         
        //protected virtual ICType RewriteApplication(ApplicationSyntaxNode syntaxNode) {
        //    return SyntaxNode.CreateApplication(
        //        Rewrite(syntaxNode.Procedure),
        //        (Pair)syntaxNode.Arguments.Map(Rewrite));
        //}

        //protected virtual ICType RewriteDefinition(DefinitionSyntaxNode syntaxNode) {
        //    return SyntaxNode.CreateDefinition(syntaxNode.Variable,
        //                               Rewrite(syntaxNode.Value));
        //}

        //protected virtual ICType RewriteExpansion(ExpansionSyntaxNode syntaxNode) {
        //    return SyntaxNode.CreateExpansion(syntaxNode.Value);
        //}

        //protected virtual ICType RewriteLambda(LambdaSyntaxNode syntaxNode) {
        //    return SyntaxNode.CreateLambda(
        //        Rewrite(syntaxNode.Parameters),
        //        Rewrite(SyntaxNode.CreateSequence(syntaxNode.Body)));
        //}

        //protected virtual ICType RewriteWhile(WhileSyntaxNode syntaxNode) {
        //    return SyntaxNode.CreateWhile(Rewrite(syntaxNode.Condition),
        //        Rewrite(syntaxNode.Condition));
        //}

        //protected virtual ICType RewriteLiteral(LiteralSyntaxNode syntaxNode) {
        //    return SyntaxNode.CreateLiteral(syntaxNode.Value);
        //}

        //protected virtual ICType RewriteSequence(SequenceSyntaxNode syntaxNode) {
        //    return SyntaxNode.CreateSequence(syntaxNode.Expressions.Map(Rewrite));
        //}

        //protected virtual ICType RewriteAssignment(AssignmentSyntaxNode syntaxNode) {
        //    return SyntaxNode.CreateAssignment(
        //        (Symbol)Rewrite(syntaxNode.Variable),
        //        Rewrite(syntaxNode.Value));
        //}

        //protected virtual ICType RewriteVariable(VariableSyntaxNode syntaxNode) {
        //    return SyntaxNode.CreateVariable(syntaxNode.Variable);
        //}
    }
}