using System;
using System.Collections.Generic;
using CursiveScript.CTypes;
using CursiveScript.Interpretation;
using CursiveScript.Interpretation.Binding;
using CursiveScript.Runtime.Functions;
using CursiveScript.Syntax.Nodes;
using CursiveScript.Transformation;
using Void = CursiveScript.CTypes.Void;

namespace CursiveScript.Macros
{
    public class MacroExpander : BasicRewriter
    {
        private Interpreter _interpreter;
        private InterpreterEnvironment _interpreterEnv;
        private PatternBinderFactory _parameterBinder;
        private HashSet<Symbol> _macrosSet; 

        public MacroExpander() {
            _macrosSet = new HashSet<Symbol>();
            _interpreterEnv = new InterpreterEnvironment();
            _interpreter = new Interpreter(_interpreterEnv);
            _parameterBinder = new PatternBinderFactory();
        }


        //protected override ICType RewriteList(Pair list) {
        //    if (list.Car.IsType(CType.Symbol) && _macrosSet.Contains((Symbol)list.Car)) {
        //        return RewriteMacroApplication(list);
        //    } else if (MacroSyntaxNode.Is(list)) {
        //        return RewriteMacroDefine(new MacroSyntaxNode(list));
        //    }

        //    return base.RewriteList(list);    
        //}


        //protected virtual ICType RewriteMacroDefine(MacroSyntaxNode syntaxNode) {
        //    var binder = _parameterBinder.Create(syntaxNode.Parameters);

        //    _interpreterEnv.Define(syntaxNode.Variable).Value = new MacroFunction(
        //        _interpreter,
        //        _interpreterEnv,
        //        syntaxNode.Body,
        //        binder);

        //    _macrosSet.Add(syntaxNode.Variable);

        //    return Void.Singleton;
        //}

        //protected virtual ICType RewriteMacroApplication(Pair list) {
        //    RunTimeFunction fn = (RunTimeFunction) _interpreterEnv.Lookup((Symbol) list.Car);
        //    ICType result = fn.Apply((Pair) list.Cdr);

        //    return Rewrite(result);
        //}
    }
}