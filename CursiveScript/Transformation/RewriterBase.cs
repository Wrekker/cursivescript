﻿using System;
using System.Collections.Generic;
using CursiveScript.Analysis;
using CursiveScript.CTypes;
using CursiveScript.Syntax.Nodes;
using Symbol = CursiveScript.CTypes.Symbol;

namespace CursiveScript.Transformation
{
    public abstract class RewriterBase {
        protected RewriterBase() { }

        protected ICType Rewrite(ICType expression) {
            if (expression.IsNull()) {
                return RewriteNull();
            }
            
            ICType res = null;

            switch (expression.Type) {
                case CType.Char:
                case CType.Real:
                    res = RewriteConstantBase(expression);
                    break;
                case CType.Symbol:
                    res = RewriteSymbolBase((Symbol)expression);
                    break;
                case CType.Pair:
                    var lst = (Pair)expression;
                    res = RewriteListBase(Pair.Create(
                            Rewrite(lst.Car),
                            Rewrite(lst.Cdr)));
                    break;
                case CType.Void:
                    res = expression;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return res;
        }
        
        protected abstract ICType RewriteNull();
        protected abstract ICType RewriteConstantBase(ICType expression);
        protected abstract Symbol RewriteSymbolBase(Symbol sym);
        protected abstract ICType RewriteListBase(Pair list);
    }
}