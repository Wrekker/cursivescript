using System;
using System.Collections.Generic;
using CursiveScript.CTypes;

namespace CursiveScript.Transformation
{
    public class RewriterEnvironment
    {
        private readonly Stack<Dictionary<Symbol, CType>> _envStack;

        public RewriterEnvironment() {
            _envStack = new Stack<Dictionary<Symbol, CType>>();
            _envStack.Push(new Dictionary<Symbol, CType>());
        }

        public RewriterEnvironment Extend() {
            _envStack.Push(new Dictionary<Symbol, CType>());
            return new SubRewriterEnvironment(this);
        }

        public void Define(Symbol name, CType type) {
            _envStack.Peek().Add(name, type);
        }

        public CType Lookup(Symbol name) {
            CType val;
            if (_envStack.Peek().TryGetValue(name, out val)) {
                return val;
            }

            return CType.None;
        }

        private class SubRewriterEnvironment : RewriterEnvironment, IDisposable
        {
            private readonly RewriterEnvironment _parent;

            public SubRewriterEnvironment(RewriterEnvironment parent) {
                _parent = parent;
            }

            public void Dispose() {
                _parent._envStack.Pop();
            }
        }
    }
}