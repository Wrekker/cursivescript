using System.Collections.Generic;
using CursiveScript.CTypes;
using CursiveScript.Syntax.Nodes;

namespace CursiveScript.Transformation
{
    public class DefinitionRearranger : BasicRewriter
    {
        protected override ICType RewriteListBase(Pair list) {
            if (!SequenceSyntaxNode.Is(list)) {
                return base.RewriteListBase(list);
            }

            List<ICType> exprs = new List<ICType>();
            List<ICType> defs = new List<ICType>();
            List<ICType> sets = new List<ICType>();
            List<ICType> macros = new List<ICType>();

            var syntaxNode = new SequenceSyntaxNode(list);

            foreach (var s in syntaxNode.Expressions) {
                if (DefinitionSyntaxNode.Is(s)) {
                    var def = new DefinitionSyntaxNode((Pair)s);
                    if (LambdaSyntaxNode.Is(def.Value)) {
                        defs.Add(SyntaxNode.CreateDefinition(def.Variable, Pair.Null));
                        sets.Add(SyntaxNode.CreateAssignment(def.Variable, def.Value));
                    } else {
                        exprs.Add(Rewrite(s));
                    }
                } else if (MacroSyntaxNode.Is(s)) {
                    macros.Add(Rewrite(s));
                } else {
                    exprs.Add(Rewrite(s));
                }
            }

            return  SyntaxNode.CreateSequence(
                defs.ToPairs()
                .Append(macros.ToPairs())
                .Append(sets.ToPairs())
                .Append(exprs.ToPairs()));
        }
    }
}