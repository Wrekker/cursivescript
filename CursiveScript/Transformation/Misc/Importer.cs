using System.IO;
using CursiveScript.CTypes;
using CursiveScript.Syntax.Nodes;
using CursiveScript.Syntax.Parsing;

namespace CursiveScript.Transformation
{
    public class Importer : BasicRewriter
    {
        private static readonly Symbol ImportSymbol = Symbol.Create("#import");
        private readonly Parser _parser;

        public Importer() {
            _parser = new Parser();
        }

        protected override ICType RewriteListBase(Pair list) {
            if (!list.Car.IsType(CType.Symbol) || list.Car != ImportSymbol) {
                return base.RewriteListBase(list);
            }

            dynamic args = list.Cdr;

            string path = args.Car.Cdr.Car.ToString();
            string body = File.ReadAllText(path.Trim('\"'));

            Tokenizer tkz = new Tokenizer(body);
            return Rewrite(_parser.Parse(tkz));
        }

        //protected override ICType RewriteApplication(ApplicationSyntaxNode syntaxNode) {
        //    if (!syntaxNode.Procedure.IsType(CType.Symbol) || syntaxNode.Procedure != ImportSymbol) {
        //        return base.RewriteApplication(syntaxNode);
        //    }

        //    dynamic args = syntaxNode.Arguments;

        //    string path = args.Car.Cdr.Car.ToString();
        //    string body = File.ReadAllText(path.Trim('\"'));

        //    Tokenizer tkz = new Tokenizer(body);
        //    return Rewrite(_parser.Parse(tkz));
        //}
    }
}