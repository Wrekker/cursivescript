using CursiveScript.Analysis.ExecutionProcedures;
using CursiveScript.CTypes;

namespace CursiveScript.Runtime.Functions
{
    public class NativeRunTimeFunction : RunTimeFunction
    {
        private readonly RunTimeEnvironment _rtEnv;
        private readonly ExecProc _body;
        
        public override CType Type { get { return CType.Native; } }

        public NativeRunTimeFunction(RunTimeEnvironment rtEnv, ExecProc body) {
            _rtEnv = rtEnv;
            _body = body;
        }

        public override ICType Apply(Pair args) {
            var env2 = _rtEnv.Extend(args);
            return _body.Call(env2);
        }

        public override string ToString() {
            return "NativeFunction";
        }
    }
}