﻿using System;
using CursiveScript.CTypes;

namespace CursiveScript.Runtime.Functions.Native
{
    public class LogicalBinaryNativeFunction<TParam> : RunTimeFunction
        where TParam : ICType
    {
        private readonly Func<TParam, TParam, bool> _native;

        public override CType Type { get { return CType.Native; } }

        public LogicalBinaryNativeFunction(Func<TParam, TParam, bool> native) {
            _native = native;
        }

        public override ICType Apply(Pair args) {
            var argA = args.Car;
            var argB = ((Pair)args.Cdr).Car;
            
            return _native((TParam) argA, (TParam) argB) ? Symbol.True : Symbol.False;
        }
    }
}