using System;
using System.Collections.Generic;
using CursiveScript.CTypes;

namespace CursiveScript.Runtime.Functions.Native
{
    public class VariableLengthNativeFunction : RunTimeFunction
    {
        private readonly Func<IEnumerable<ICType>, ICType> _native;

        public override CType Type { get { return CType.Native; } }

        public VariableLengthNativeFunction(Func<IEnumerable<ICType>, ICType> native) {
            _native = native;
        }

        public override ICType Apply(Pair args) {
            return _native(args);
        }
    }
}