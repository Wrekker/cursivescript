using System;
using CursiveScript.CTypes;

namespace CursiveScript.Runtime.Functions.Native
{
    public class UnaryNativeFunction<TParam> : RunTimeFunction
        where TParam : ICType
    {
        private readonly Func<TParam, ICType> _native;

        public override CType Type { get { return CType.Native; } }

        public UnaryNativeFunction(Func<TParam, ICType> native) {
            _native = native;
        }

        public override ICType Apply(Pair args) {
            return _native((TParam) args.Car);
        }
    }
}