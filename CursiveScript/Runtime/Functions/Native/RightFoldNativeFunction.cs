﻿using System;
using CursiveScript.CTypes;

namespace CursiveScript.Runtime.Functions.Native
{
    public class RightFoldNativeFunction<TParam> : RunTimeFunction
        where TParam : ICType
    {
        private readonly Func<TParam, TParam, TParam> _native;
        private readonly TParam _identity;

        public override CType Type { get { return CType.Native; } }

        public RightFoldNativeFunction(Func<TParam, TParam, TParam> native, TParam identity) {
            _native = native;
            _identity = identity;
        }
        
        public override ICType Apply(Pair args) {
            if (args.Cdr.IsNull()) {
                return _native(_identity, (TParam)args.Car);
            }

            return RightFold(args);
        }

        private ICType RightFold(Pair args) {
            if (args.IsNull()) {
                return _identity;
            }

            return _native((TParam) args.Car, (TParam)RightFold((Pair)args.Cdr));
        }
    }
}