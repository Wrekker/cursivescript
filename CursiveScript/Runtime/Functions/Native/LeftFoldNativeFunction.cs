﻿using System;
using CursiveScript.CTypes;

namespace CursiveScript.Runtime.Functions.Native
{
    public class LeftFoldNativeFunction<TParam> : RunTimeFunction
        where TParam : ICType
    {
        private readonly Func<TParam, TParam, TParam> _native;
        private readonly TParam _identity;

        public override CType Type { get { return CType.Native; } }

        public LeftFoldNativeFunction(Func<TParam, TParam, TParam> native, TParam identity) {
            _native = native;
            _identity = identity;
        }
        
        public override ICType Apply(Pair args) {
            if (args.Cdr.IsNull()) {
                return _native(_identity, (TParam)args.Car);
            }

            var acc = args.Car;
            foreach (var arg in (Pair)args.Cdr) {
                acc = _native((TParam)acc, (TParam)arg);
            }

            return acc;
        }
    }
}