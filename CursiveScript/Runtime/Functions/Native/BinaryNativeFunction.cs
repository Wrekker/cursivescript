using System;
using CursiveScript.CTypes;

namespace CursiveScript.Runtime.Functions.Native
{
    public class BinaryNativeFunction<TParam, TParam2> : RunTimeFunction
        where TParam : ICType
    {
        private readonly Func<TParam, TParam2, ICType> _native;

        public override CType Type { get { return CType.Native; } }

        public BinaryNativeFunction(Func<TParam, TParam2, ICType> native) {
            _native = native;
        }

        public override ICType Apply(Pair args) {
            var argA = args.Car;
            var argB = ((Pair)args.Cdr).Car;
            
            return _native((TParam) argA, (TParam2) argB);
        }
    }
}