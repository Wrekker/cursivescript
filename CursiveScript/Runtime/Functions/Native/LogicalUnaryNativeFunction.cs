using System;
using CursiveScript.CTypes;

namespace CursiveScript.Runtime.Functions.Native
{
    public class LogicalUnaryNativeFunction<TParam> : RunTimeFunction
        where TParam : ICType
    {
        private readonly Func<TParam, bool> _native;

        public override CType Type { get { return CType.Native; } }

        public LogicalUnaryNativeFunction(Func<TParam, bool> native) {
            _native = native;
        }

        public override ICType Apply(Pair args) {
            var argA = args.Car;
            
            return _native((TParam) argA) ? Symbol.True : Symbol.False;
        }
    }
}