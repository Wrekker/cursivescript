using System;
using CursiveScript.CTypes;

namespace CursiveScript.Runtime.Functions.Native
{
    public class Logical3BinaryNativeFunction<TParam> : RunTimeFunction
        where TParam : ICType
    {
        private readonly Func<TParam, TParam, bool?> _native;

        public override CType Type { get { return CType.Native; } }

        public Logical3BinaryNativeFunction(Func<TParam, TParam, bool?> native) {
            _native = native;
        }

        public override ICType Apply(Pair args) {
            var argA = args.Car;
            var argB = ((Pair)args.Cdr).Car;
            
            var ret = _native((TParam) argA, (TParam) argB);
            
            if (ret == null) {
                return Pair.Null;
            }
            return ret.Value ? Symbol.True : Symbol.False;
        }
    }
}