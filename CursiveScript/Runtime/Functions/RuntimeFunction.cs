﻿using CursiveScript.CTypes;
using CursiveScript.Interpretation;

namespace CursiveScript.Runtime.Functions
{
    public abstract class RunTimeFunction : ICType
    {
        public virtual CType Type { get { return CType.Procedure; } }
        public virtual ICType DefaultValue { get { return null; } }
        
        public abstract ICType Apply(Pair args);
    }
}