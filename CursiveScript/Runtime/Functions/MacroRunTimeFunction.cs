using CursiveScript.Analysis;
using CursiveScript.Analysis.ExecutionProcedures;
using CursiveScript.Binding;
using CursiveScript.CTypes;

namespace CursiveScript.Runtime.Functions
{
    public class MacroRunTimeFunction : RunTimeFunction
    {
        public RunTimeEnvironment Environment { get; private set; }
        public ExecProc Body { get; private set; }
        public ICType Parameters { get; private set; }
        public IParameterBinder<RunTimeEnvironment> ParameterBinder { get; private set; }

        public override CType Type { get { return CType.Macro; } }
        public override ICType DefaultValue { get { return new MacroRunTimeFunction(null, null, null, null); } }

        public MacroRunTimeFunction(RunTimeEnvironment env, ExecProc body, IParameterBinder<RunTimeEnvironment> parameterBinder, ICType parameters) {
            Environment = env;
            Body = body;
            ParameterBinder = parameterBinder;
            Parameters = parameters;
        }

        public override ICType Apply(Pair args) {
            RunTimeEnvironment env2 = this.Environment.Extend();
            this.ParameterBinder.Bind(env2, args);
            return this.Body.Call(env2);
        }

        public override string ToString() {
            return Type + "(" + string.Join(", ", Parameters) + ")";
        }
    }
}