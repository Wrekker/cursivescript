using CursiveScript.CTypes;

namespace CursiveScript.Runtime
{
    public class RunTimeBinding
    {
        public ICType Value { get; set; }

        public RunTimeBinding(ICType value) {
            Value = value;
        }
    }
}