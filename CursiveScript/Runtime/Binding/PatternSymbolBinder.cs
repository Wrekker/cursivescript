using CursiveScript.Binding;
using CursiveScript.CTypes;

namespace CursiveScript.Runtime.Binding
{
    public class PatternSymbolBinder : IParameterBinder<RunTimeEnvironment>
    {
        public void Bind(RunTimeEnvironment env, ICType arguments) {
            env.Define(arguments);
        }
    }
}