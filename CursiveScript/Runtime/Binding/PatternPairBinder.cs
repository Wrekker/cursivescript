using CursiveScript.Binding;
using CursiveScript.CTypes;

namespace CursiveScript.Runtime.Binding
{
    public class PatternPairBinder : IParameterBinder<RunTimeEnvironment>
    {
        public IParameterBinder<RunTimeEnvironment> CarBinder { get; set; }
        public IParameterBinder<RunTimeEnvironment> CdrBinder { get; set; }

        public PatternPairBinder(IParameterBinder<RunTimeEnvironment> car, IParameterBinder<RunTimeEnvironment> cdr) {
            CarBinder = car;
            CdrBinder = cdr;
        }

        public void Bind(RunTimeEnvironment env, ICType arguments) {
            if (arguments.IsEmpty()) {
                arguments = Void.Singleton.Cons(Void.Singleton);
            }

            CarBinder.Bind(env, ((Pair) arguments).Car);
            CdrBinder.Bind(env, ((Pair) arguments).Cdr);
        }
    }
}