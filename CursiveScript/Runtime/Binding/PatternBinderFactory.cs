﻿using CursiveScript.Binding;
using CursiveScript.CTypes;

namespace CursiveScript.Runtime.Binding
{
    public class PatternBinderFactory : IParameterBinderFactory<RunTimeEnvironment>
    {
        public IParameterBinder<RunTimeEnvironment> Create(ICType expr) {
            if (expr.IsNull()) {
                return new PatternNullBinder();
            }

            if (expr.IsType(CType.Symbol)) {
                return new PatternSymbolBinder();
            }

            return new PatternPairBinder(
                Create(((Pair) expr).Car), 
                Create(((Pair) expr).Cdr));
        }

    }
}