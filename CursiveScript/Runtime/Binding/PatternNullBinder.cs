using CursiveScript.Binding;
using CursiveScript.CTypes;

namespace CursiveScript.Runtime.Binding
{
    public class PatternNullBinder : IParameterBinder<RunTimeEnvironment>
    {
        public void Bind(RunTimeEnvironment env, ICType arguments) { }
    }
}