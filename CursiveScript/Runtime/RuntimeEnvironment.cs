﻿using System;
using System.Collections.Generic;
using System.Linq;
using CursiveScript.Addressing;
using CursiveScript.CTypes;

namespace CursiveScript.Runtime
{
    public class RunTimeEnvironment
    {
        private Tuple<List<RunTimeBinding>, object> _frames;
        private int _depth = 0;

        public int Count { get { return _frames.Item1.Count; } }

        public RunTimeEnvironment() {
            _frames = new Tuple<List<RunTimeBinding>, object>(new List<RunTimeBinding>(), null);
        }

        private RunTimeEnvironment(RunTimeEnvironment enclosing) {
            _frames = new Tuple<List<RunTimeBinding>, object>(new List<RunTimeBinding>(), enclosing._frames);
            _depth = enclosing._depth + 1;
        }

        private RunTimeBinding Resolve(LexicalAddress adr) {
            var f = _frames;
            var frameDepth = adr.FrameDepth;

            for(var i=_depth;i > frameDepth;--i)
                f = (Tuple<List<RunTimeBinding>, object>)f.Item2;

            if (adr.BindingIndex < f.Item1.Count) {
                return f.Item1[adr.BindingIndex];
            }

            //return new RunTimeBinding(Pair.Null);
            throw new Exception("Reference to unbound variable: " + adr.Name);
        }

        public void Define(ICType val) {
            _frames.Item1.Add(new RunTimeBinding(val));
        }

        public ICType Lookup(LexicalAddress adr) {
            RunTimeBinding binding = Resolve(adr);
            return binding.Value;
        }

        public void LookupArray(int count, ICType[] destination) {
            var bindings = _frames.Item1;
            var bindingsCount = bindings.Count;

            for (int i = 0; i < count; i++) {
                destination[count - i - 1] = bindings[bindingsCount - i - 1].Value;
            }
        }

        public ICType[] LookupParameters() {
            int parameterCount = _frames.Item1.Count;
            ICType[] parameters = new ICType[parameterCount];

            LookupArray(parameterCount, parameters);
            return parameters;
        }

        public void Set(LexicalAddress name, ICType value) {
            RunTimeBinding b = Resolve(name);
            b.Value = value;
        }

        public RunTimeEnvironment Extend() { return Extend(null); }
        public RunTimeEnvironment Extend(IEnumerable<ICType> args) {
            var env = new RunTimeEnvironment(this);

            if (args == null) {
                return env;
            }

            foreach (var a in args) {
                env.Define(a);
            }

            return env;
        }
    }
}