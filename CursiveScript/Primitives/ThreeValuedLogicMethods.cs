﻿using CursiveScript.CTypes;
using Symbol = CursiveScript.CTypes.Symbol;

namespace CursiveScript.Primitives
{
    public static class ThreeValuedLogicMethods
    {
        public static ICType Plus(ICType a, ICType b) {
            if (a.IsNull() || b.IsNull()) { return null; }
            return ((Fraction) a) + ((Fraction) b);
        }
        
        public static ICType Minus(ICType a, ICType b) {
            if (a.IsNull() || b.IsNull()) { return null; }
            return ((Fraction) a) - ((Fraction) b);
        }

        public static ICType Multiply(ICType a, ICType b) {
            if (a.IsNull() || b.IsNull()) { return null; }
            return ((Fraction) a) * ((Fraction) b);
        }

        public static ICType Divide(ICType a, ICType b) {
            if (a.IsNull() || b.IsNull()) { return null; }
            return ((Fraction) a) / ((Fraction) b);
        }
        
        public static bool? LessThan(ICType a, ICType b) {
            if (a.IsNull() || b.IsNull()) { return null; }
            return ((Fraction) a) < ((Fraction) b);
        }

        public static bool? GreaterThan(ICType a, ICType b) {
            if (a.IsNull() || b.IsNull()) { return null; }
            return ((Fraction) a) > ((Fraction) b);
        }

        public static bool? Equal(ICType a, ICType b) {
            if (a.IsNull() || b.IsNull()) { return null; }
            return ((Fraction) a) == ((Fraction) b);
        }

        public static bool? Inequal(ICType a, ICType b) {
            if (a.IsNull() || b.IsNull()) { return null; }
            return ((Fraction) a) != ((Fraction) b);
        }

        public static bool? LessThanOrEq(ICType a, ICType b) {
            if (a.IsNull() || b.IsNull()) { return null; }
            return ((Fraction) a) <= ((Fraction) b);
        }

        public static bool? GreaterThanOrEq(ICType a, ICType b) {
            if (a.IsNull() || b.IsNull()) { return null; }
            return ((Fraction) a) >= ((Fraction) b);
        }         

        public static ICType And(ICType a, ICType b) {
            if (a.IsNull() || b.IsNull()) { return null; }
            return (a.IsTrue() && b.IsTrue()) ? Symbol.True : Symbol.False;
        }         

        public static ICType Or(ICType a, ICType b) {
            if (a.IsNull() && b.IsNull()) { return null; }
            return (a.IsTrue() || b.IsTrue()) ? Symbol.True : Symbol.False;
        }         

    }
}