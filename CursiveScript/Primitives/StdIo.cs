﻿using System.IO;

namespace CursiveScript.Primitives
{
    public static class StdIo
    {
        public static TextWriter Out { get; set; }

        static StdIo() {
            Out = TextWriter.Null;    
        }

        public static void WriteLine(string text) {
            Out.WriteLine(text);
        }
    }
}