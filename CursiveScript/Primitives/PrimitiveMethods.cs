using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using CursiveScript.Analysis;
using CursiveScript.CTypes;
using CursiveScript.Runtime;
using Symbol = CursiveScript.CTypes.Symbol;
using Void = CursiveScript.CTypes.Void;

namespace CursiveScript.Primitives
{
    public static class PrimitiveMethods
    {
        public static Void SetCdr(Pair pair, ICType value) {
            pair.Cdr = value;
            return Void.Singleton;
        }

        public static Void SetCar(Pair pair, ICType value) {
            pair.Car = value;
            return Void.Singleton;
        }

        public static Pair Cons(ICType car, ICType cdr) {
            return car.Cons(cdr);
        }

        public static ICType Car(Pair pair) { return pair.Car; }
        public static ICType Cdr(Pair pair) { return pair.Cdr; }

        public static bool IsTrue(ICType value) {
            return (value is Symbol ? value != Symbol.False : !value.IsNull());
        }

        public static bool IsNull(ICType value) {
            return value.IsNull();
        }

        public static Pair List(IEnumerable<ICType> values) {
            return values.ToPairs();
        }

        public static ICType Append(IEnumerable<ICType> lists) {
            if (!lists.Any()) {
                return Pair.Null;
            }

            Pair
                head = Pair.Create(null, null), 
                tail = head;

            foreach (var list in lists) {
                var subList = (Pair) list;

                while (!subList.IsNull()) {
                    tail.Cdr = subList.Car.Cons(null);
                    tail = (Pair)tail.Cdr;
                    subList = (Pair)subList.Cdr;
                }
            }

            return head.Cdr;
        }

        public static bool AreEqual(ICType a, ICType b) {
            if (a == null || b == null) { return a == b; }

            if (a.GetType() != b.GetType()) { return false; }

            if (a.IsType(CType.Symbol)) { return a.Equals(b); }
            if (a.IsType(CType.Real)) { return a == b; }
            if (a.IsType(CType.Char)) { return a.Equals(b); }

            return ReferenceEquals(a, b);
        }

        readonly static Regex _displayFormat = new Regex(@"(?<!%)%(?<n>\d+)");
        public static Void Display(IEnumerable<ICType> values) {
            var valueArray = values.ToArray();
            
            if (valueArray.Length == 0) {
                return Void.Singleton;
            }

            string text = "()";

            if (!valueArray[0].IsNull()) {
                 text = _displayFormat.Replace(
                    valueArray[0].ToString(),
                    m => {
                        int idx = int.Parse(m.Groups["n"].Value);
                        if (valueArray[idx + 1] == null) {
                            return string.Empty;
                        }
                        return valueArray[idx + 1].ToString();
                    });

                text = text.Replace("%%", "%");
            }

            StdIo.WriteLine(text);
            return Void.Singleton;
        }

        public static bool IsPair(ICType a) { return a.IsType(CType.Pair); }
        public static bool IsNumber(ICType a) { return a.IsType(CType.Real); }
        public static bool IsSymbol(ICType a) { return a.IsType(CType.Symbol); }
        public static bool IsProcedure(ICType a) { return a.IsType(CType.Procedure); }
        public static bool IsMacro(ICType a) { return a.IsType(CType.Macro); }
        public static bool IsString(ICType a) { return a.IsType(CType.Pair) && ((Pair)a).IsProperListOf(CType.Char); }
        public static bool IsEmpty(ICType a) { return a.IsVoid(); }
        public static bool Not(ICType a) { return !a.IsTrue(); }
    }
}