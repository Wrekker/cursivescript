﻿using System.Collections.Generic;
using CursiveScript.CTypes;
using CursiveScript.Runtime.Functions.Native;

namespace CursiveScript.Primitives
{
    public class PrimitiveScript
    {
        public static readonly Dictionary<Symbol, ICType> NativeProcs = new Dictionary<Symbol, ICType> {
            //{ Symbol.AddReal, new LeftFoldNativeFunction<Fraction>((a, b) => a + b, new Fraction(0)) },
            //{ Symbol.SubtractReal, new LeftFoldNativeFunction<Fraction>((a, b) => a - b, new Fraction(0)) },
            //{ Symbol.MultiplyReal, new LeftFoldNativeFunction<Fraction>((a, b) => a * b, new Fraction(1)) }, 
            //{ Symbol.DivideReal, new LeftFoldNativeFunction<Fraction>((a, b) => a / b, new Fraction(1)) },

            //{ Symbol.LessThan, new LogicalBinaryNativeFunction<Fraction>((a, b) => a < b) },
            //{ Symbol.GreaterThan, new LogicalBinaryNativeFunction<Fraction>((a, b) => a > b) },
            //{ Symbol.LessThanEqual, new LogicalBinaryNativeFunction<Fraction>((a, b) => a <= b) },
            //{ Symbol.GreaterThanEqual, new LogicalBinaryNativeFunction<Fraction>((a, b) => a >= b) },
            //{ Symbol.EqualReal, new LogicalBinaryNativeFunction<Fraction>((a, b) => a == b) },
            //{ Symbol.InequalReal, new LogicalBinaryNativeFunction<Fraction>((a, b) => a != b) },

            { Symbol.AddReal, new LeftFoldNativeFunction<ICType>(ThreeValuedLogicMethods.Plus, new Fraction(0)) },
            { Symbol.SubtractReal, new LeftFoldNativeFunction<ICType>(ThreeValuedLogicMethods.Minus, new Fraction(0)) },
            { Symbol.MultiplyReal, new LeftFoldNativeFunction<ICType>(ThreeValuedLogicMethods.Multiply, new Fraction(1)) }, 
            { Symbol.DivideReal, new LeftFoldNativeFunction<ICType>(ThreeValuedLogicMethods.Divide, new Fraction(1)) },

            { Symbol.LessThan, new Logical3BinaryNativeFunction<ICType>(ThreeValuedLogicMethods.LessThan) },
            { Symbol.GreaterThan, new Logical3BinaryNativeFunction<ICType>(ThreeValuedLogicMethods.GreaterThan) },
            { Symbol.LessThanEqual, new Logical3BinaryNativeFunction<ICType>(ThreeValuedLogicMethods.LessThanOrEq) },
            { Symbol.GreaterThanEqual, new Logical3BinaryNativeFunction<ICType>(ThreeValuedLogicMethods.GreaterThanOrEq) },
            { Symbol.EqualReal, new Logical3BinaryNativeFunction<ICType>(ThreeValuedLogicMethods.Equal) },
            { Symbol.InequalReal, new Logical3BinaryNativeFunction<ICType>(ThreeValuedLogicMethods.Inequal) },

            { Symbol.AreEqual, new LogicalBinaryNativeFunction<ICType>(PrimitiveMethods.AreEqual) },

            { Symbol.Not, new LogicalUnaryNativeFunction<ICType>(PrimitiveMethods.Not) },
            { Symbol.IsNull, new LogicalUnaryNativeFunction<ICType>(PrimitiveMethods.IsNull) },
            { Symbol.IsPair, new LogicalUnaryNativeFunction<ICType>(PrimitiveMethods.IsPair) },
            { Symbol.IsNumber, new LogicalUnaryNativeFunction<ICType>(PrimitiveMethods.IsNumber) },
            { Symbol.IsSymbol, new LogicalUnaryNativeFunction<ICType>(PrimitiveMethods.IsSymbol) },
            { Symbol.IsProcedure, new LogicalUnaryNativeFunction<ICType>(PrimitiveMethods.IsProcedure) },
            { Symbol.IsMacro, new LogicalUnaryNativeFunction<ICType>(PrimitiveMethods.IsMacro) },
            
            { Symbol.IsString, new LogicalUnaryNativeFunction<ICType>(PrimitiveMethods.IsString) },
            { Symbol.IsEmpty, new LogicalUnaryNativeFunction<ICType>(PrimitiveMethods.IsEmpty) },
            
            { Symbol.Cons, new BinaryNativeFunction<ICType, ICType>(PrimitiveMethods.Cons) },
            { Symbol.Car, new UnaryNativeFunction<Pair>(PrimitiveMethods.Car) },
            { Symbol.Cdr, new UnaryNativeFunction<Pair>(PrimitiveMethods.Cdr) },
            { Symbol.SetCar, new BinaryNativeFunction<Pair, ICType>(PrimitiveMethods.SetCar) },
            { Symbol.SetCdr, new BinaryNativeFunction<Pair, ICType>(PrimitiveMethods.SetCdr) },

            { Symbol.List, new VariableLengthNativeFunction(PrimitiveMethods.List) },
            { Symbol.Append, new VariableLengthNativeFunction(PrimitiveMethods.Append) },
            { Symbol.Display, new VariableLengthNativeFunction(PrimitiveMethods.Display) },

            { Symbol.True, Symbol.True },
            { Symbol.False, Symbol.False },
            { Symbol.Null, Pair.Null }
        };
    }
}