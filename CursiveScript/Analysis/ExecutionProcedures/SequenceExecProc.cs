using System.Collections.Generic;
using CursiveScript.CTypes;
using CursiveScript.Runtime;

namespace CursiveScript.Analysis.ExecutionProcedures.Native
{
    class SequenceExecProc : ExecProc
    {
        private readonly IList<ExecProc> _sequence;

        public SequenceExecProc(IList<ExecProc> sequence) : base() {
            _sequence = sequence;
        }

        public override ICType Call(RunTimeEnvironment re) {
            if (_sequence.Count == 0) {
                return Void.Singleton;
            }

            for (int i = 0; i < _sequence.Count - 1; i++) {
                _sequence[i].Call(re);
            }

            return _sequence[_sequence.Count - 1].Call(re);
        }
    }
}