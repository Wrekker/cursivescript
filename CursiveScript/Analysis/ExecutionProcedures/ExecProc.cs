﻿using System.Collections;
using System.Runtime.InteropServices;
using System.Threading;
using CursiveScript.CTypes;
using CursiveScript.Runtime;

namespace CursiveScript.Analysis.ExecutionProcedures
{
    public class ExecProc
    {
        private ExecProcDelegate _proc;

        protected ExecProc() {}
        public ExecProc(ExecProcDelegate proc) {
            _proc = proc;
        }

        public virtual ICType Call(RunTimeEnvironment re) {
            return _proc(re);
        }
    }
}