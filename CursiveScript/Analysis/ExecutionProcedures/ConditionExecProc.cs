using CursiveScript.CTypes;
using CursiveScript.Runtime;

namespace CursiveScript.Analysis.ExecutionProcedures
{
    class ConditionExecProc : ExecProc
    {
        private ExecProc _trueBranch;
        private ExecProc _falseBranch;
        private ExecProc _predicate;

        public ConditionExecProc(ExecProc predicate, ExecProc trueBranch, ExecProc falseBranch) {
            _predicate = predicate;
            _trueBranch = trueBranch;
            _falseBranch = falseBranch;
        }

        public override ICType Call(RunTimeEnvironment re) {
            if (_predicate.Call(re).IsTrue())
                return _trueBranch.Call(re);

            return _falseBranch.Call(re);
        }
    }
}