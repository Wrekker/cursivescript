using CursiveScript.CTypes;
using CursiveScript.Runtime;

namespace CursiveScript.Analysis.ExecutionProcedures
{
    public delegate ICType ExecProcDelegate(RunTimeEnvironment e);
}