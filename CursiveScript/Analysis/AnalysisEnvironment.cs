﻿using System;
using System.Collections.Generic;
using System.Linq;
using CursiveScript.Addressing;
using CursiveScript.Analysis.ExecutionProcedures;
using CursiveScript.CTypes;

namespace CursiveScript.Analysis
{
    public class AnalysisEnvironment
    {
        private readonly Tuple<List<AnalysisTimeBinding>, object> _frames;
        
        private int _depth = 0;

        public int Count { get { return _frames.Item1.Count; } }

        public AnalysisEnvironment() {
            _frames = new Tuple<List<AnalysisTimeBinding>, object>(new List<AnalysisTimeBinding>(), null);
        }

        private AnalysisEnvironment(AnalysisEnvironment enclosing) {
            _frames = new Tuple<List<AnalysisTimeBinding>, object>(new List<AnalysisTimeBinding>(), enclosing._frames);
            _depth = enclosing._depth + 1;
        }

        private AnalysisTimeBinding Resolve(Symbol name) {
            var f = _frames;
            
            while (f != null) {
                var bindings = f.Item1;
                for (int i = bindings.Count - 1; i >= 0; i--) {
                    if (bindings[i].ResolvesAs(name)) {
                        return bindings[i];
                    }
                }

                f = (Tuple<List<AnalysisTimeBinding>, object>)f.Item2;
            }

            //return new AnalysisTimeBinding(Symbol.Null, new LexicalAddress(Symbol.Null, int.MaxValue, int.MaxValue));
            throw new Exception("Reference to unbound variable: " + name);
        }

        public void Define(Symbol name) {
            if (_frames.Item1.Any(a => a.ResolvesAs(name))) {
                throw new Exception(name + " is already defined.");
            }

            var adr = new LexicalAddress(name, _depth, _frames.Item1.Count);
            _frames.Item1.Add(new AnalysisTimeBinding(name, adr));
        }

        public LexicalAddress Lookup(Symbol name) {
            AnalysisTimeBinding binding = Resolve(name);
            return binding.Address;
        }

        public Symbol[] Lookup(int count) {
            List<Symbol> ret = new List<Symbol>();
            
            var bindings = _frames.Item1;
            for (int i = bindings.Count - count; i < bindings.Count; i++) {
                ret.Add(bindings[i].Name);
            }

            return ret.ToArray();
        }

        public AnalysisEnvironment Extend() { return Extend(null); }
        public AnalysisEnvironment Extend(IEnumerable<ICType> parameters) {
            var env = new AnalysisEnvironment(this);

            if (parameters == null) {
                return env;
            }

            foreach (var p in parameters) {
                env.Define((Symbol)p);
            }
            
            return env;
        }
        
        
    }
}