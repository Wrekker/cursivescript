using CursiveScript.Addressing;
using CursiveScript.CTypes;

namespace CursiveScript.Analysis
{
    public class AnalysisTimeBinding
    {
        public Symbol Name { get; private set; }
        public LexicalAddress Address { get; private set; }

        public AnalysisTimeBinding(Symbol name, LexicalAddress address) {
            Name = name;
            Address = address;
        }

        public bool ResolvesAs(Symbol name) {
            return this.Name.Equals(name);
        }
        
        public override string ToString() {
            return Name + ": " + Address;
        }
    }
}