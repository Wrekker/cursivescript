﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Linq;
using System.Numerics;
using CursiveScript.Analysis.ExecutionProcedures;
using CursiveScript.Binding;
using CursiveScript.CTypes;
using CursiveScript.Interpretation;
using CursiveScript.Interpretation.Functions;
using CursiveScript.Macros;
using CursiveScript.Primitives;
using CursiveScript.Runtime;
using CursiveScript.Runtime.Binding;
using CursiveScript.Runtime.Functions;
using CursiveScript.Runtime.Functions.Native;
using CursiveScript.Syntax.Nodes;
using SequenceExecProc = CursiveScript.Analysis.ExecutionProcedures.Native.SequenceExecProc;
using Symbol = CursiveScript.CTypes.Symbol;
using Void = CursiveScript.CTypes.Void;

namespace CursiveScript.Analysis
{
    public class Analyzer
    {
        private readonly PatternBinderFactory _patternBinderFactory = new PatternBinderFactory();

        private static readonly ExecProc EmptyProc = new ExecProc(env => Void.Singleton);
        private static readonly ExecProc NullProc = new ExecProc(env => Pair.Null);
        
        public Analyzer(AnalysisEnvironment cEnv, RunTimeEnvironment reEnv) {
            foreach (var nf in PrimitiveScript.NativeProcs) {
                cEnv.Define(nf.Key);
                reEnv.Define(nf.Value);
            }
        }
        public ExecProc Analyze(ICType expr, AnalysisEnvironment cEnv) {
            if (expr.IsNull()) { return NullProc; }
            if (expr.IsEmpty()) { return EmptyProc; }

            switch (expr.Type) {
                case CType.Char:
                case CType.Real:
                    return AnalyzeSelfEvaluating(new SelfEvaluatingSyntaxNode(expr), cEnv);
                case CType.Symbol:
                    return AnalyzeVariable((Symbol)expr, cEnv);
                case CType.Continuation:
                    return ((Continuation) expr).ContinueWith;
                case CType.Pair:
                    var lst = (Pair)expr;

                    if (SequenceSyntaxNode.Is(expr)) { return AnalyzeSequence(new SequenceSyntaxNode(lst), cEnv); }
                    if (AssignmentSyntaxNode.Is(expr)) { return AnalyzeAssignment(new AssignmentSyntaxNode(lst), cEnv); }
                    if (DefinitionSyntaxNode.Is(expr)) { return AnalyzeDefinition(new DefinitionSyntaxNode(lst), cEnv); }
                    if (MacroSyntaxNode.Is(expr)) { return AnalyzeMacroDefinition(new MacroSyntaxNode(lst), cEnv); }
                    if (LambdaSyntaxNode.Is(expr)) { return AnalyzeLambda(new LambdaSyntaxNode(lst), cEnv); }
                    if (ConditionSyntaxNode.Is(expr)) { return AnalyzeCondition(new ConditionSyntaxNode(lst), cEnv); }
                    if (WhileSyntaxNode.Is(expr)) { return AnalyzeWhile(new WhileSyntaxNode(lst), cEnv); }
                    if (LiteralSyntaxNode.Is(expr)) { return AnalyzeQuote(new LiteralSyntaxNode(lst), cEnv); }
                    

                    return AnalyzeApplication(new ApplicationSyntaxNode(lst), cEnv);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public ExecProc AnalyzeCondition(ConditionSyntaxNode expr, AnalysisEnvironment cEnv) {
            var p = Analyze(expr.Predicate, cEnv);
            var t = Analyze(expr.TrueBranch, cEnv);
            var f = Analyze(expr.FalseBranch, cEnv);

            return new ConditionExecProc(p,t,f);
        }

        public ExecProc AnalyzeLambda(LambdaSyntaxNode expr, AnalysisEnvironment cEnv) {
            var cEnv2 = cEnv.Extend(expr.Parameters.Flatten());

            ExecProc body = Analyze(expr.Body, cEnv2);
            IParameterBinder<RunTimeEnvironment> binder = _patternBinderFactory.Create(expr.Parameters);

            return new ExecProc(env => new LambdaRunTimeFunction(env, body, binder, expr.Parameters));
        }

        private ExecProc AnalyzeMacroDefinition(MacroSyntaxNode expr, AnalysisEnvironment cEnv) {
            cEnv.Define(expr.Variable);
            var cEnv2 = cEnv.Extend(expr.Parameters.Flatten());

            ExecProc body = Analyze(expr.Body, cEnv2);
            IParameterBinder<RunTimeEnvironment> binder = _patternBinderFactory.Create(expr.Parameters);

            return new ExecProc(env => {
                env.Define(new MacroRunTimeFunction(env, body, binder, expr.Parameters));
                return Void.Singleton;
            });
        }

        public ExecProc AnalyzeDefinition(DefinitionSyntaxNode expr, AnalysisEnvironment cEnv) {
            var variable = expr.Variable;
            cEnv.Define(variable);
            var value = Analyze(expr.Value, cEnv);

            return new ExecProc(env => {
                var argValue = value.Call(env);

                env.Define(argValue);
                return Void.Singleton;
            });
        }

        public ExecProc AnalyzeAssignment(AssignmentSyntaxNode expr, AnalysisEnvironment cEnv) {
            var adr = cEnv.Lookup(expr.Variable);
            var value = Analyze(expr.Value, cEnv);

            return new ExecProc(env => {
                var argValue = value.Call(env);

                env.Set(adr, argValue);
                return Void.Singleton;
            });
        }

        public ExecProc AnalyzeSelfEvaluating(SelfEvaluatingSyntaxNode expr, AnalysisEnvironment cEnv) {
            return new ExecProc(env => expr.Value);
        }

        public ExecProc AnalyzeVariable(Symbol expr, AnalysisEnvironment cEnv) {
            var adr = cEnv.Lookup(expr);
            return new ExecProc(env => env.Lookup(adr));
        }

        public ExecProc AnalyzeApplication(ApplicationSyntaxNode expr, AnalysisEnvironment cEnv) {
            ExecProc func = Analyze(expr.Procedure, cEnv);
            ExecProc[] argProcsCache = null;
            
            ExecProc macroCache = null;

            return new ExecProc(env => {
                RunTimeFunction fn = (RunTimeFunction)func.Call(env);

                if (fn.Type == CType.Macro) {
                    if (macroCache == null) {
                        macroCache = Analyze(fn.Apply(expr.Arguments), cEnv);
                    }
                    var ret = macroCache.Call(env);
                    return ret;
                } else if (argProcsCache == null) {
                    argProcsCache = expr.Arguments
                        .MapToEnumerable((v, t) => Analyze(v, cEnv))
                        .ToArray();
                }

                Pair args = argProcsCache.Map(m => m.Call(env));
                return fn.Apply(args);
            });
        }

        public ExecProc AnalyzeSequence(SequenceSyntaxNode expr, AnalysisEnvironment cEnv) {
            if (expr.Expressions == Pair.Null) {
                return EmptyProc;
            }
            
            List<ExecProc> seq = new List<ExecProc>();
            foreach (var ex in expr.Expressions) {
                seq.Add(Analyze(ex, cEnv));
            }

            return new SequenceExecProc(seq.ToArray());
        }
        private ExecProc AnalyzeWhile(WhileSyntaxNode expr, AnalysisEnvironment cEnv) {
            ExecProc condition = Analyze(expr.Condition, cEnv);
            ExecProc body = Analyze(expr.Body, cEnv);

            return new ExecProc((env) => {
                ICType ret = Void.Singleton;
                while (condition.Call(env).IsTrue()) {
                    ret = body.Call(env);
                }
                return ret;
            });
        }

        public ExecProc AnalyzeQuote(LiteralSyntaxNode expr, AnalysisEnvironment cEnv) {
            return AnalyzeLiteral(expr.Value, cEnv);
        }

        public ExecProc AnalyzeLiteral(ICType expr, AnalysisEnvironment cEnv) {
            if (expr.IsNull()) {
                return NullProc;
            }

            switch (expr.Type) {
                case CType.Symbol:                    
                case CType.Char:
                case CType.Real:
                    return AnalyzeSelfEvaluating(new SelfEvaluatingSyntaxNode(expr), cEnv);
                case CType.Pair:
                    var lst = (Pair) expr;
                          
                    if (LiteralSyntaxNode.Is(lst)) {
                        return AnalyzeSelfEvaluating(new SelfEvaluatingSyntaxNode(((Pair)lst.Cdr).Car), cEnv);
                    }

                    return AnalyzeLiteralList((Pair)expr, cEnv);
            }
            
            throw new Exception();
        }
        
        public ExecProc AnalyzeLiteralExpansion(ExpansionSyntaxNode expr, AnalysisEnvironment cEnv) {
            if (expr.Value.IsNull()) { return NullProc; }

            return Analyze(expr.Value, cEnv);
        }

        public ExecProc AnalyzeLiteralList(Pair lst, AnalysisEnvironment cEnv) {
            if (lst == Pair.Null) { return NullProc; }

            ExecProc head = null;
            ExecProc tail;
            if (lst.Cdr.IsType(CType.Pair)) {
                tail = AnalyzeLiteralList((Pair) lst.Cdr, cEnv); 
            } else {
                tail = AnalyzeLiteral(lst.Cdr, cEnv);
            }

            if (SpliceSyntaxNode.Is(lst.Car)) {
                return AnalyzeLiteralSplice(new SpliceSyntaxNode(lst.Car), tail, cEnv);
            } else if (ExpansionSyntaxNode.Is(lst.Car)) {
                head = AnalyzeLiteralExpansion(new ExpansionSyntaxNode(lst.Car), cEnv);
            } else {
                head = AnalyzeLiteral(lst.Car, cEnv);
            }

            return new ExecProc(env => head.Call(env).Cons(tail.Call(env)));
        }
        
        public ExecProc AnalyzeLiteralSplice(SpliceSyntaxNode expr, ExecProc tail, AnalysisEnvironment cEnv) {
            var head = AnalyzeLiteralExpansion(new ExpansionSyntaxNode(expr.SourceSyntax), cEnv);
            
            return new ExecProc(env => {
                var headRes = head.Call(env);
                var tailRes = tail.Call(env);

                if (headRes.IsNull()) {
                    return tailRes;
                }

                if (headRes.IsType(CType.Pair)) {
                    return ((Pair)headRes).Append(tailRes);
                }

                return headRes.Cons(tailRes);
            });
        }
    }
}