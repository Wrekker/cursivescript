using CursiveScript.CTypes;
using CursiveScript.Runtime;

namespace CursiveScript.Addressing
{
    public class LexicalAddress
    {
        public Symbol Name { get; private set; } // For debugging purposes
        public int FrameDepth { get; private set; }
        public int BindingIndex { get; private set; }
        
        public LexicalAddress(Symbol name, int frameDepth, int bindingIndex) {
            Name = name;
            FrameDepth = frameDepth;
            BindingIndex = bindingIndex;
        }

        public override string ToString() {
            return Name + ": " + FrameDepth + "." + BindingIndex;
        }
    }
}