﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CursiveScript.CTypes
{
    public static class CTypeExtensions
    {
        public delegate ICType Mapper(ICType value, bool isTail);
        public delegate T Mapper<out T>(ICType value, bool isTail);
        public delegate void Aggregator(ICType value, bool isTail);


        public static ICType ToPairs(this ICType[] list) {
            ICType ret = Pair.Null;
            for (int i = list.Length - 1; i >= 0; i--) {
                ret = list[i].Cons(ret);
            }
            return ret;
        }

        public static Pair ToPairs(this IEnumerable<ICType> list) {
            if (list == null) {
                return Pair.Null;
            }

            var head = Pair.Create(null, null);
            var tail = head;

            foreach (var p in list) {
                tail.Cdr = Pair.Create(p, Pair.Null);
                tail = (Pair)tail.Cdr;
            }

            return (Pair)head.Cdr;
        }

        public static IList<ICType> ToList(this Pair pair) {
            IList<ICType> ret = new List<ICType>();

            Pair it = pair;
            while (it != Pair.Null) {
                ret.Add(it.Car);
                it = (Pair)it.Cdr;
            }

            return ret;
        }

        public static ICType Map(this Pair t, Mapper mapper) {
            if (t.IsNull()) {
                return Pair.Null;
            }

            if (t.Cdr.IsType(CType.Atom)) {
                return mapper(t.Car, false).Cons(mapper(t.Cdr, true));
            }
            
            return mapper(t.Car, false).Cons(Map((Pair) t.Cdr, mapper));
        }

        public static ICType Map(this Pair pairs, Func<ICType, ICType> mapper) {
            if (pairs.IsNull()) {
                return pairs;
            }

            return Pair.Create(mapper(pairs.Car), Map((Pair) pairs.Cdr, mapper));
        }

        public static ICType MapTree(this ICType tree, Func<ICType, ICType> mapper) {
            if (tree.IsNull()) {
                return tree;
            }

            if (tree.IsList()) {
                return Pair.Create(
                    mapper(((Pair)tree).Car), 
                    MapTree(((Pair)tree).Cdr, mapper));
            }

            return mapper(tree);
        }

        public static Pair Map<T>(this IEnumerable<T> pairs, Func<T, ICType> mapper) {
            var head = Pair.Create(null, null);
            var tail = head;

            foreach (var p in pairs) {
                tail.Cdr = Pair.Create(mapper(p), Pair.Null);
                tail = (Pair) tail.Cdr;
            }

            return (Pair)head.Cdr;
        }

        public static IEnumerable<T> MapToEnumerable<T>(this Pair t, Mapper<T> mapper)
        {
            if (t.IsNull()) { return Enumerable.Empty<T>(); }

            List<T> ret = new List<T>();
            foreach (var v in t) {
                ret.Add(mapper(v, false));
            }

            return ret;
        }

        public static bool IsProperList(this Pair t) {
            return IsProperListOf(t, CType.None);
        }

        public static bool IsProperListOf(this Pair t, CType type) {
            if (t.IsNull()) {
                return true;
            }

            return t.Car.IsType(type) 
                && IsProperListOf((Pair) t.Cdr, type);
        }

        public static CType IsProperListOf(this Pair t) {
            if (t.IsNull() || t.Car.IsNull()) {
                return CType.None;
            }

            CType type = t.Car.Type;
            if (IsProperListOf(t, type)) {
                return type;
            }

            return CType.None;
        }

        public static Pair Append(this Pair a, ICType b) {
            if (a.IsNull()) {
                return (Pair)b;
            }

            if (a.Cdr.IsNull()) {
                return a.Car.Cons(b);
            }

            if (a.Cdr.IsType(CType.Atom)) {
                return a.Cons(b);
            }

            return a.Car.Cons(Append((Pair)a.Cdr, b));
        }
        
        public static Pair Flatten(this ICType expr) {
            if (expr.IsNull()) {
                return Pair.Null;
            }

            if (!expr.IsList()) {
                return expr.Cons(Pair.Null);
            }

            return Flatten(((Pair) expr).Car).Append(Flatten(((Pair) expr).Cdr));
        }

        public static bool IsTagged(this ICType t, Symbol tag) {
            return t.IsType(CType.Pair)
                && ((Pair)t).Car.IsType(CType.Symbol)
                && ((Pair)t).Car.Equals(tag);
        }

        public static bool IsNull(this ICType t) {
            return t  == null || t == Pair.Null;
        }

        public static bool IsList(this ICType t) {
            return t.IsNull() || t.IsType(CType.Pair);
        }

        public static bool IsEmpty(this ICType t) {
            return t.IsNull() || t.Type == CType.Void;
        }

        public static bool IsVoid(this ICType t) {
            return !t.IsNull() && t.Type == CType.Void;
        }

        public static bool IsType(this ICType t, CType type) {
            return !t.IsNull() && (t.Type & type) == type;
        }

        public static bool IsTrue(this ICType t) {
            return !t.IsEmpty() &&
                   !t.IsNull() &&
                   t != Symbol.False;
        }

        public static Pair Cons(this ICType car, ICType cdr) {
            return Pair.Create(car, cdr);
        }

        public static ICType Conl(this ICType car, ICType cdr) {
            return car.Cons(cdr.Cons(Pair.Null));
        }

        public static ICType Last(this Pair t) {
            if (t.IsEmpty()) {
                return t;
            }
            
            if (t.Cdr.IsType(CType.Atom) || t.Cdr.IsNull()) {
                return t;
            }

            return ((Pair)t.Cdr).Last();
        }



        public static ICType[] ToArray(this Pair pairs) {
            List<ICType> ret =  new List<ICType>();
            Pair p = pairs;

            while (!p.IsNull()) {
                ret.Add(p.Car);
                p = (Pair)p.Cdr;
            }

            return ret.ToArray();
        }
    }
}