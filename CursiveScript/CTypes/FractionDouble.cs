﻿using System.Numerics;

namespace CursiveScript.CTypes
{
    public class Fraction : ICType
    {
        private double _num;
        
        public Fraction() : this(0, 1) { }
        
        public Fraction(BigInteger numerator, BigInteger denominator) {
            _num = (double) (numerator / denominator);

            //Normalize();
        }

        public Fraction(decimal value) {
            _num = (double) value;
        }
        
        public CType Type { get { return CType.Real; } }

        public static implicit operator Fraction(int value) { return new Fraction(value, 1); }
        public static implicit operator Fraction(decimal value) { return new Fraction(value); }
        public static implicit operator Fraction(double value) { return new Fraction((decimal)value); }

        public static explicit operator decimal(Fraction value) {
            return ((decimal)value._num);
        }

        public static explicit operator decimal?(Fraction value) {
            if (ReferenceEquals(value, null)) {
                return null;
            }
            return (decimal) value;
        }
        
        public static Fraction operator +(Fraction a, Fraction b) {
            return a._num + b._num;
        }

        public static Fraction operator *(Fraction a, Fraction b) {
            return a._num * b._num;
        }

        public static Fraction operator /(Fraction a, Fraction b) {
            return a._num / b._num;
        }

        public static Fraction operator -(Fraction a, Fraction b) {
            return a._num - b._num;
        }

        public static bool operator >(Fraction a, Fraction b) {
            return a._num > b._num;
        }

        public static bool operator <=(Fraction a, Fraction b) {
            return a == b || a < b;
        }

        public static bool operator >=(Fraction a, Fraction b) {
            return a == b || a > b;
        }

        public static bool operator <(Fraction a, Fraction b) {
            return a != b && !(a > b);
        }

        public static bool operator ==(Fraction a, Fraction b) {
            return a._num == b._num;
        }

        public static bool operator !=(Fraction a, Fraction b) {
            return !(a == b);
        }
        
        public override string ToString() {
            return _num.ToString();
        }

        public bool Equals(Fraction other) {
            return other._num.Equals(_num);
        }

        public override int GetHashCode() {
            unchecked {
                return (_num.GetHashCode()*397);
            }
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            if (obj.GetType() != typeof (Fraction)) {
                return false;
            }
            return Equals((Fraction) obj);
        }
    }
}