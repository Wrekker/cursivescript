﻿namespace CursiveScript.CTypes
{
    public partial class Symbol
    {
        public static readonly Symbol Sequence = Create("begin");
        public static readonly Symbol Assignment = Create("set!");
        public static readonly Symbol Definition = Create("define");
        public static readonly Symbol Lambda = Create("lambda");
        public static readonly Symbol Condition = Create("if");
        public static readonly Symbol Literal = Create("quote");
        public static readonly Symbol Macro = Create("define-macro");
        public static readonly Symbol Expansion = Create("@expand");
        public static readonly Symbol Splice = Create("@splice");
                       
        public static readonly Symbol True = Create("#t");
        public static readonly Symbol False = Create("#f");
        public static readonly Symbol Null = Create("null");
                       
        public static readonly Symbol AddReal = Create("+");
        public static readonly Symbol SubtractReal = Create("-");
        public static readonly Symbol DivideReal = Create("/");
        public static readonly Symbol MultiplyReal = Create("*");
        public static readonly Symbol LessThan = Create("<");
        public static readonly Symbol GreaterThan = Create(">");
        public static readonly Symbol LessThanEqual = Create("<=");
        public static readonly Symbol GreaterThanEqual = Create(">=");
        public static readonly Symbol EqualReal = Create("=");
        public static readonly Symbol InequalReal = Create("/=");
                       
        public static readonly Symbol Cons = Create("cons");
        public static readonly Symbol List = Create("list");
        public static readonly Symbol Append = Create("append");
        public static readonly Symbol Car = Create("car");
        public static readonly Symbol Cdr = Create("cdr");
        public static readonly Symbol SetCar = Create("set-car!");
        public static readonly Symbol SetCdr = Create("set-cdr!");
        
        public static readonly Symbol AreEqual = Create("eq?");
        public static readonly Symbol Not = Create("not");
        public static readonly Symbol IsNull = Create("null?");
        public static readonly Symbol IsPair = Create("pair?");
        public static readonly Symbol IsNumber = Create("number?");
        public static readonly Symbol IsSymbol = Create("symbol?");
        public static readonly Symbol IsProcedure = Create("procedure?");
        public static readonly Symbol IsMacro = Create("macro?");
        public static readonly Symbol IsString = Create("string?");
        public static readonly Symbol IsEmpty = Create("empty?");
        public static readonly Symbol Display = Create("display");
        public static readonly Symbol While = Create("while");
    }
}