﻿using System;
using CursiveScript.Analysis.ExecutionProcedures;

namespace CursiveScript.CTypes
{
    [Flags]
    public enum CType
    {
        None = 0x0,
        
        Real = 0x1 | Atom,
        Procedure = 0x2,
        Pair = 0x4,
        Void = 0x8,
        Symbol = 0x10 | Atom,
        Macro = 0x20 | Procedure,
        Char = 0x40 | Atom,
        Continuation = 0x80,
        Native = 0x100 | Procedure,

        Atom = 0x800,
    }
}