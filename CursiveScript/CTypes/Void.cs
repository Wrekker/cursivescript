namespace CursiveScript.CTypes
{
    public class Void : ICType
    {
        public CType Type { get { return CType.Void; } }

        public static readonly Void Singleton = new Void();

        private Void() { }

        public override string ToString() {
            return "<void>";
        }
    }
}