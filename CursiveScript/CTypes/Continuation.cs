﻿using CursiveScript.Analysis.ExecutionProcedures;
using CursiveScript.Runtime;

namespace CursiveScript.CTypes
{
    public class Continuation : ICType
    {
        public ExecProc ContinueWith { get; private set; }
        public RunTimeEnvironment Environment { get; set; }

        public CType Type { get { return CType.Continuation; } }

        public Continuation(ExecProc continueWith, RunTimeEnvironment re) {
            ContinueWith = continueWith;
            Environment = re;
        }
    }
}