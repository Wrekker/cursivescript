﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CursiveScript.Runtime;

namespace CursiveScript.CTypes
{
    public class Pair : IEnumerable<ICType>, ICType
    {
        public ICType Car;
        public ICType Cdr;
        
        public CType Type { get { return CType.Pair; } }

        public static readonly Pair Null = null; //new Pair(null, null);

        private Pair(ICType car, ICType cdr) {
            this.Car = car;
            this.Cdr = cdr;
        }

        public static Pair Create(ICType car, ICType cdr) {
            return new Pair(car, cdr);
        }

        public static bool operator false(Pair t) { return t.Equals(Pair.Null); }
        public static bool operator true(Pair t) { return !t.Equals(Pair.Null); }
        
        public override string ToString() {
            if (this.IsNull()) {
                return "()";
            }

            if (this.IsProperListOf(CType.Char)) {
                return "\"" + new string(this.Select(s => ((Character) s).Value).ToArray()) + "\"";
            }

            return ToString(true, 100);
        }

        private string ToString(bool openParen, int depth) {
            if (depth <= 0) {
                return "...)";
            }

            StringBuilder sb = new StringBuilder(16);
            if (openParen) {
                sb.Append("(");
            }

            sb.Append(Car);

            if (Cdr.IsNull()) {
                sb.Append(")");
            } else if (Cdr.IsType(CType.Pair)) {
                sb.Append(" ").Append(((Pair)Cdr).ToString(false, depth - 1));
            } else if (Cdr != Pair.Null) {
                sb.Append(" . ").Append(Cdr).Append(")");
            }

            return sb.ToString();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }

        public IEnumerator<ICType> GetEnumerator() {
            ICType c = this;
            while (!c.IsNull()) {
                yield return ((Pair)c).Car;
                c = ((Pair)c).Cdr;
            }
        }
    }


}
