using System;
using System.Collections.Generic;

namespace CursiveScript.CTypes
{
    public partial class Symbol : ICType
    {
        private static Dictionary<string, Symbol> _symbols;

        public string Value { get; private set; }
        public CType Type { get { return CType.Symbol; } }
        public ICType DefaultValue { get { return new Symbol(string.Empty); } }
        
        private Symbol(string value) {
            Value = value;
        }

        public static bool operator false(Symbol s) { return s.Equals(Symbol.False); }
        public static bool operator true(Symbol s) { return !s.Equals(Symbol.False); }

        public static Symbol Create(string name) {
            if (_symbols == null) {
                _symbols = new Dictionary<string, Symbol>(StringComparer.InvariantCultureIgnoreCase);
            }

            Symbol value;
            if (!_symbols.TryGetValue(name, out value)) {
                value = new Symbol(name);
                _symbols.Add(name, value);
            }
            return value;
        }
        
        public override string ToString() {
            return Value;
        }

        public bool Equals(Symbol s) {
            if (s == null) {
                return false;
            }

            return ReferenceEquals(this, s);
        }

        public override bool Equals(object obj) {
            if (obj == null || typeof(Symbol) != obj.GetType()) {
                return false;
            }

            return ReferenceEquals(this, obj);

            //return this.Value.Equals(((Symbol) obj).Value,
            //                        StringComparison.InvariantCultureIgnoreCase);
        }
        
        public override int GetHashCode() {
            return (Value != null ? Value.GetHashCode() : 0);
        }
    }
}