﻿namespace CursiveScript.CTypes
{
    public interface ICType
    {
        CType Type { get; }
    }
}