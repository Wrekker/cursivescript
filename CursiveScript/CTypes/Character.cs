﻿using System.Globalization;

namespace CursiveScript.CTypes
{
    public struct Character : ICType
    {
        public char Value { get; private set; }

        public CType Type { get { return CType.Char; } }

        public Character(char character) : this() {
            Value = character;
        }
        
        public override string ToString() {
            return "\\" + this.Value.ToString(CultureInfo.InvariantCulture);
        }

        public override bool Equals(object obj) {
            if (obj is Character) {
                return this.Value.Equals(((Character)obj).Value);
            }
            return false;
        }

        public override int GetHashCode() {
            return Value.GetHashCode();
        }
    }
}