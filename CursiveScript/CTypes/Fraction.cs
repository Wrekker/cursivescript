﻿using System.Numerics;

namespace CursiveScript.CTypes
{
    public class Fraction : ICType
    {
        private BigInteger _num;
        private BigInteger _denom;
        
        public Fraction() : this(0, 1) { }
        
        public Fraction(BigInteger numerator, BigInteger denominator) {
            _num = numerator;
            _denom = denominator;

            //Normalize();
        }

        public Fraction(decimal value) {
            _denom = 1;
            
            while (decimal.Truncate(value) < value) {
                _denom *= 10;
                value *= 10;
            }

            _num = (BigInteger)value;

            //Normalize();
        }
        
        public CType Type { get { return CType.Real; } }

        public static implicit operator Fraction(int value) { return new Fraction(value, 1); }
        public static implicit operator Fraction(decimal value) { return new Fraction(value); }
        public static implicit operator Fraction(double value) { return new Fraction((decimal)value); }

        public static explicit operator decimal(Fraction value) {
            var normalizedValue = value.Normalize();
            return  ((decimal) normalizedValue._num)/((decimal) normalizedValue._denom);
        }

        public static explicit operator decimal?(Fraction value) {
            if (ReferenceEquals(value, null)) {
                return null;
            }
            return (decimal) value;
        }
        
        public static Fraction operator +(Fraction a, Fraction b) {
            BigInteger nom = (a._num*b._denom) + (b._num*a._denom);
            BigInteger denom = a._denom * b._denom;

            return new Fraction(nom, denom);
        }

        public static Fraction operator *(Fraction a, Fraction b) {
            BigInteger nom = a._num * b._num;
            BigInteger denom = a._denom * b._denom;

            return new Fraction(nom, denom);
        }

        public static Fraction operator /(Fraction a, Fraction b) {
            BigInteger nom = a._num * b._denom;
            BigInteger denom = a._denom * b._num;

            return new Fraction(nom, denom);
        }

        public static Fraction operator -(Fraction a, Fraction b) {
            BigInteger nom = (a._num * b._denom) - (b._num * a._denom);
            BigInteger denom = a._denom * b._denom;

            return new Fraction(nom, denom);
        }

        public static bool operator >(Fraction a, Fraction b) {
            BigInteger numA = (a._num * b._denom);
            BigInteger numB = (b._num * a._denom);

            return numA > numB;
        }
        public static bool operator ==(Fraction a, Fraction b) {
            BigInteger numA = (a._num * b._denom);
            BigInteger numB = (b._num * a._denom);

            return numA == numB;
        }

        public static bool operator <=(Fraction a, Fraction b) {
            return a == b || a < b;
        }

        public static bool operator >=(Fraction a, Fraction b) {
            return a == b || a > b;
        }

        public static bool operator <(Fraction a, Fraction b) {
            return a != b && !(a > b);
        }


        public static bool operator !=(Fraction a, Fraction b) {
            return !(a == b);
        }

        private Fraction Normalize() {
            var gcd = BigInteger.GreatestCommonDivisor(_num, _denom);

            if (gcd > 1) {
                return new Fraction(_num / gcd, _denom / gcd);
            }

            return this;
        }

        //private static Fraction Normalize(BigInteger nom, BigInteger denom) {
        //    var gcd = BigInteger.GreatestCommonDivisor(nom, denom);

        //    if (gcd > 1) {
        //        nom /= gcd;
        //        denom /= gcd;
        //    }

        //    return new Fraction(nom, denom);
        //}

        public override string ToString() {
            var normalized = this.Normalize();

            if (normalized._denom == 1) {
                return normalized._num.ToString();
            }

            var wholes = (int)(normalized._num / normalized._denom);
            if (wholes > 0) {
                return string.Format(
                    "{0}+{1}/{2}",
                    wholes,
                    (normalized._num % normalized._denom),
                    normalized._denom);
            }

            return string.Format("{0}/{1}", normalized._num, normalized._denom);
        }

        public bool Equals(Fraction other) {
            return other._num.Equals(_num) && other._denom.Equals(_denom);
        }

        public override int GetHashCode() {
            unchecked {
                return (_num.GetHashCode()*397) ^ _denom.GetHashCode();
            }
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            if (obj.GetType() != typeof (Fraction)) {
                return false;
            }
            return Equals((Fraction) obj);
        }
    }
}