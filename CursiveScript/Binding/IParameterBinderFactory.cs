using CursiveScript.CTypes;

namespace CursiveScript.Binding
{
    public interface IParameterBinderFactory<TEnv>
    {
        IParameterBinder<TEnv> Create(ICType expr);
    }
}