using CursiveScript.CTypes;

namespace CursiveScript.Binding
{
    public delegate void BindingProc<TEnv>(TEnv env, ICType arguments);
}