using CursiveScript.CTypes;

namespace CursiveScript.Binding
{
    public interface IParameterBinder<TEnv>
    {
        void Bind(TEnv env, ICType arguments);
    }
}