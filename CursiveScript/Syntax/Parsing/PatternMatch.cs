namespace CursiveScript.Syntax.Parsing
{
    public class PatternMatch
    {
        public Pattern Pattern { get; set; }
        public string Text { get; set; }

        public PatternMatch(Pattern pat, string text) {
            Pattern = pat;
            Text = text;
        }
    }
}