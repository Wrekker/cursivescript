using System;

namespace CursiveScript.Syntax.Parsing
{
    [Serializable]
    public class ParseException : Exception
    {
        public ParseException(string msg) : base(msg) {
            
        }
    }
}