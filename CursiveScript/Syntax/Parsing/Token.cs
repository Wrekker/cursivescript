namespace CursiveScript.Syntax.Parsing
{
    public struct Token
    {
        public TokenType TokenType;
        public string Data;
        //public int Character;
        public int Line;

        public Token(TokenType type, string data, int line) {
            TokenType = type;
            Data = data;
            //Character = character;
            Line = line;
        }

        public override string ToString() {
            return Data;
        }
    }
}