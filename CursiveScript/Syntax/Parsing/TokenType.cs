﻿using System;

namespace CursiveScript.Syntax.Parsing
{
    [Flags]
    public enum TokenType
    {
        None = 0x0,
        Float = 0x1,
        Fraction = 0x2,
        Symbol = 0x4,
        LParen = 0x8,
        RParen = 0x10,
        Literal = 0x20,
        Expand = 0x40,
        Period = 0x80,
        String = 0x100,
        Char = 0x200,
        Splice = 0x400
    }
}