﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Numerics;
using CursiveScript.CTypes;
using CursiveScript.Syntax.Nodes;
using Void = CursiveScript.CTypes.Void;

namespace CursiveScript.Syntax.Parsing
{
    public class Parser
    {
        private int _depth;

        public ICType Parse(Tokenizer tokenizer) {
            _depth = 0;

            List<ICType> lines = new List<ICType>();
            while (true) {
                var next = ParseBody(tokenizer);
                if (next.IsVoid()) {
                    break;
                }

                lines.Add(next);
            }

            return SyntaxNode.CreateSequence(lines.ToPairs());
        }

        private ICType ParseBody(Tokenizer tokenizer) {
            Token t = tokenizer.Next();

            switch (t.TokenType) {
                case TokenType.None:
                    return Void.Singleton;
                case TokenType.Float:
                    return ParseReal(t);
                case TokenType.Fraction:
                    return ParseFraction(t);
                case TokenType.Symbol:
                    return ParseSymbol(t);
                case TokenType.LParen:
                    _depth++;
                    return ParseList(tokenizer);
                case TokenType.Literal:
                    return ParseLiteral(tokenizer);
                case TokenType.Expand:
                    return ParseExpansion(tokenizer);
                case TokenType.Splice:
                    return ParseSplicing(tokenizer);
                case TokenType.String:
                    return ParseString(t);
                case TokenType.Char:
                    return ParseChar(t);
                default:
                    throw ThrowContextualException(t);
            }
        }

        private Exception ThrowContextualException(Token t) {
            if (t.TokenType == TokenType.RParen) {
                throw new ParseException("*** Unmatched right parenthesis on line " + t.Line);
            }
            if (_depth > 0) {
                return new ParseException("*** Missing right parenthesis on line " + t.Line);
            }
            return new ParseException(string.Format("*** Unknown token, {0}, on line {1}.", t.Data, t.Line));
        }

        private ICType ParseChar(Token token) {
            return new Character(token.Data[1]);
        }

        private ICType ParseString(Token token) {
            ICType ret = Pair.Null;
            for (int i = token.Data.Length - 2; i > 0; i--) {
                ret = new Character(token.Data[i]).Cons(ret);
            }

            return SyntaxNode.CreateLiteral(ret);
        }

        private ICType ParseLiteral(Tokenizer tokenizer) {
            return SyntaxNode.CreateLiteral(ParseBody(tokenizer));
        }

        private ICType ParseExpansion(Tokenizer tokenizer) {
            return SyntaxNode.CreateExpansion(ParseBody(tokenizer));
        }

        private ICType ParseSplicing(Tokenizer tokenizer) {
            return SyntaxNode.CreateSplicing(ParseBody(tokenizer));
        }

        private ICType ParseList(Tokenizer tokenizer) {
            return RecurseList(tokenizer);
        }

        private ICType RecurseList(Tokenizer tokenizer) {
            if (tokenizer.Accept(TokenType.RParen)) {
                _depth--;
                return Pair.Null;
            }

            if (tokenizer.Accept(TokenType.Period)) { 
                var ret = ParseBody(tokenizer); 
                tokenizer.Expect(TokenType.RParen);
                return ret;
            }

            return ParseBody(tokenizer).Cons(RecurseList(tokenizer));
        }

        private ICType ParseSymbol(Token t) {
            return Symbol.Create(t.Data);
        }

        private ICType ParseReal(Token t) {
            return new Fraction(Decimal.Parse(t.Data, CultureInfo.GetCultureInfo("en-US")));
        }

        private ICType ParseFraction(Token t) {
            string[] noms = t.Data.Split('+', '/');
            if (noms.Length == 3) {
                var wholes = BigInteger.Parse(noms[0]);
                var num = BigInteger.Parse(noms[1]);
                var denom = BigInteger.Parse(noms[2]);

                return new Fraction(wholes * denom + num, denom);
            }

            return new Fraction(BigInteger.Parse(noms[0]), BigInteger.Parse(noms[1]));
        }

    }
}