﻿using System.Text.RegularExpressions;

namespace CursiveScript.Syntax.Parsing
{
    public class Pattern
    {
        private readonly Regex _rgx;

        public TokenType TokenType { get; private set; }

        public Pattern(TokenType type, string rgx) {
            _rgx = new Regex("\\G" + rgx, RegexOptions.Multiline | RegexOptions.Compiled);
            
            TokenType = type;
        }
        
        public virtual PatternMatch Match(string text, ref int position) {
            if (text.Length > 0) {
                Match m = _rgx.Match(text, position);
                if (m.Success) {
                    position += m.Length;
                    return new PatternMatch(this, m.Value);
                }
            }
            
            return null;
        }
    }
}