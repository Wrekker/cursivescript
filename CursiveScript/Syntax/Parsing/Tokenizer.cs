using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace CursiveScript.Syntax.Parsing
{
    public class Tokenizer
    {
        private readonly string _text;
        private int _position;
        private int _line;
        private Token? _peek;

        private readonly Pattern[] _patterns = new Pattern[] {
                                                       new Pattern(TokenType.Char, @"\\."),
                                                       new Pattern(TokenType.LParen, @"\("),
                                                       new Pattern(TokenType.RParen, @"\)"),
                                                       new Pattern(TokenType.Period, @"\."),
                                                       new Pattern(TokenType.Literal, @"'"),
                                                       new Pattern(TokenType.Splice, @",@"),
                                                       new Pattern(TokenType.Expand, @","),
                                                       new Pattern(TokenType.Float, @"([+-])?(?:\d*\.?\d+(?![\d/]))(?!\+)"),
                                                       new Pattern(TokenType.Fraction, @"([+-])?(?:\d*\+)?(?:\d+\/\d+)"),
                                                       new Pattern(TokenType.Symbol, @"[^@.()',\s""]+"),
                                                       new Pattern(TokenType.String, @"\""(?:[^\""]|(?:\""\""))*\""")
                                                   };

        private readonly Pattern _whitespace = new Pattern(TokenType.None, @"(?:\s+|\s*;[^\r\n]*)");
        private readonly Regex _linebreaks = new Regex(@"(?<c>(?:\r\n)+|\r+|\n+)");

        public Tokenizer(string text) {
            _position = 0;
            _text = text;
            _line = 1;
        }

        public Token Next() {
            Token ret;

            if (_peek != null) {
                ret = _peek.Value;
                _peek = null;
                return ret;
            }

            PatternMatch ws;
            while ((ws =_whitespace.Match(_text, ref _position)) != null) {
                _line += _linebreaks.Matches(ws.Text).Count;
            }

            if (_position >= _text.Length) {
                return new Token(TokenType.None, null, _line);
            }

            foreach (var pattern in _patterns) {
                PatternMatch match = pattern.Match(_text, ref _position);
                if (match != null) {
                    return new Token(match.Pattern.TokenType, match.Text, _line);
                }
            }

            string fragment = _text.Substring(_position, Math.Min(_text.Length - _position, 10));
            throw new Exception("No valid token at \"" + fragment + "...\" on line " + _line);
        }

        public Token Peek() {
            if (_peek == null) {
                _peek = Next();
            }
            return _peek.Value;
        }

        public Token Expect(params TokenType[] types) {
            var next = Peek();

            if (types.Any(a => (a & next.TokenType) == a)) {
                return Next();
            }

            throw new Exception("Unacceptable token, " + next.TokenType + ", on line " + _line);
        }

        public bool Accept(params TokenType[] types) {
            var peek = Peek();

            if (types.Any(a => (a & peek.TokenType) == a)) {
                Next();
                return true;
            }
            return false;
        }

    }
}