using CursiveScript.CTypes;

namespace CursiveScript.Syntax.Nodes
{
    public class ConditionSyntaxNode : SyntaxNode
    {
        public ICType Predicate { get; private set; }
        public ICType TrueBranch { get; private set; }
        public ICType FalseBranch { get; private set; }

        public ConditionSyntaxNode(ICType expr) : base(expr, SyntaxNodeType.Condition) {
            var dyn = expr as dynamic;

            Predicate = dyn.Cdr.Car;
            TrueBranch = dyn.Cdr.Cdr.Car;
            if (dyn.Cdr.Cdr.Cdr != Pair.Null) {
                FalseBranch = dyn.Cdr.Cdr.Cdr.Car;
            } else {
                FalseBranch = Void.Singleton;
            }
        }

        public static bool Is(ICType expression) {
            return expression.IsTagged(Symbol.Condition);
        }
    }
}