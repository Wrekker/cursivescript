﻿using CursiveScript.CTypes;

namespace CursiveScript.Syntax.Nodes
{
    public abstract partial class SyntaxNode
    {
        public SyntaxNodeType Type { get; private set; }
        public ICType SourceSyntax { get; private set; }

        public SyntaxNode(ICType sourceSyntax, SyntaxNodeType type) {
            SourceSyntax = sourceSyntax;
            Type = type;
        }

        //public abstract ExecProc Analyze(CompilationEnvironment ce);
    }
}