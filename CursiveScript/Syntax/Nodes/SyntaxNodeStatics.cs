using System.Collections.Generic;
using System.Linq;
using CursiveScript.CTypes;

namespace CursiveScript.Syntax.Nodes
{
    public partial class SyntaxNode
    {
        public static ICType CreateSequence(ICType expressions) {
            if (expressions.IsTagged(Symbol.Sequence)) {
                return expressions;
            }
            
            return Symbol.Sequence.Cons(expressions);
        }

        public static ICType CreateExpansion(ICType value) {
            return Symbol.Expansion.Conl(value);
        }

        public static ICType CreateDefinition(Symbol name, ICType value) {
            return Symbol.Definition.Cons(name.Conl(value));
        }

        public static ICType CreateLambda(ICType parameters, ICType body) {
            return Symbol.Lambda.Cons(parameters.Conl(body));
        }

        public static ICType CreateCondition(ICType condition, ICType trueBranch, ICType falseBranch) {
            return Symbol.Condition.Cons(condition.Cons(trueBranch.Conl(falseBranch)));
        }

        public static ICType CreateMacro(Symbol name, ICType parameters, ICType body) {
            var head = name.Cons(parameters);
            return Symbol.Macro.Cons(head.Conl(body));
        }

        public static ICType CreateAssignment(Symbol name, ICType value) {
            return Symbol.Assignment.Cons(name.Conl(value));
        }

        public static ICType CreateSelfEvaluation(ICType value) {
            return value;
        }

        public static ICType CreateVariable(Symbol name) {
            return name;
        }

        public static ICType CreateLiteral(ICType value) {
            return Symbol.Literal.Conl(value);
        }

        public static ICType CreateApplication(ICType proc, IEnumerable<ICType> parameters) {
            parameters = parameters ?? Enumerable.Empty<ICType>();
            return proc.Cons(parameters.ToPairs());
        }

        public static ICType CreateSplicing(ICType value) {
            return Symbol.Splice.Conl(value);
        }

        public static ICType CreateWhile(ICType condition, ICType body) {
            return Symbol.While.Cons(condition.Conl(body));
        }
    }
}