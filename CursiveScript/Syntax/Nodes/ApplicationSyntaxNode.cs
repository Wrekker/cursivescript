using CursiveScript.CTypes;

namespace CursiveScript.Syntax.Nodes
{
    public class ApplicationSyntaxNode : SyntaxNode
    {
        public ICType Procedure { get; private set; }
        public Pair Arguments { get; private set; }

        public ApplicationSyntaxNode(Pair expr) : base(expr, SyntaxNodeType.Application) {
            Procedure = expr.Car;
            Arguments = (Pair)expr.Cdr;
        }

        public static bool Is(ICType expression) {
            return expression.IsType(CType.Pair);
        }
    }
}