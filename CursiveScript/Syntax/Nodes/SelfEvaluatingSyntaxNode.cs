using CursiveScript.CTypes;

namespace CursiveScript.Syntax.Nodes
{
    public class SelfEvaluatingSyntaxNode : SyntaxNode
    {
        public ICType Value { get; private set; }

        public SelfEvaluatingSyntaxNode(ICType expr) : base(expr, SyntaxNodeType.SelfEvaluating) {
            this.Value = expr;
        }

        public static bool Is(ICType expression) {
            return expression.IsType(CType.Char) ||
                   expression.IsType(CType.Real) ||
                   expression.IsType(CType.Void);
        }
    }
}