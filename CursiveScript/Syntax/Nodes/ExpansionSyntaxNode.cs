using CursiveScript.CTypes;

namespace CursiveScript.Syntax.Nodes
{
    public class ExpansionSyntaxNode : SyntaxNode
    {
        public ICType Value { get; private set; }
        
        public ExpansionSyntaxNode(ICType expr) : base(expr, SyntaxNodeType.Expansion) {
            Value = (expr as dynamic).Cdr.Car;
        }

        public static bool Is(ICType expression) {
            return expression.IsTagged(Symbol.Expansion);
        }

        public override string ToString() {
            return "," + Value;
        }
    }
}