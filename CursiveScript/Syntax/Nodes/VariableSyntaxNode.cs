using CursiveScript.CTypes;

namespace CursiveScript.Syntax.Nodes
{
    public class VariableSyntaxNode : SyntaxNode
    {
        public CTypes.Symbol Variable { get; private set; }

        public VariableSyntaxNode(ICType expr) 
            : base(expr, SyntaxNodeType.Variable) {
            Variable = (CTypes.Symbol) expr;
        }

        public static bool Is(ICType expression) {
            return expression.IsType(CType.Symbol);
        }
    }
}