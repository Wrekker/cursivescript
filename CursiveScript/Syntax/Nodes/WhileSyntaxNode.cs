using CursiveScript.CTypes;
using CursiveScript.Syntax.Nodes;

namespace CursiveScript.Analysis
{
    public class WhileSyntaxNode : SyntaxNode
    {
        public ICType Condition { get; private set; }
        public ICType Body { get; private set; }

        public WhileSyntaxNode(ICType expr) : base(expr, SyntaxNodeType.While) {
            dynamic ex = expr;

            Condition = ex.Cdr.Car;
            Body = ex.Cdr.Cdr.Car;
        }

        public static bool Is(ICType expr) {
            return expr.IsTagged(Symbol.While);
        }
    }
}