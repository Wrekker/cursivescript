using CursiveScript.CTypes;

namespace CursiveScript.Syntax.Nodes
{
    public class LambdaSyntaxNode : SyntaxNode
    {
        public ICType Parameters { get; private set; }
        public ICType Body { get; private set; }
        
        public LambdaSyntaxNode(ICType expr) : base(expr, SyntaxNodeType.Lambda) {
            var dyn = expr as dynamic;

            Parameters = dyn.Cdr.Car;
            dyn = dyn.Cdr.Cdr;
            if (dyn.Cdr == Pair.Null && SequenceSyntaxNode.Is(dyn.Car)) {
                Body = dyn.Car;
            } else {
                Body =  SyntaxNode.CreateSequence(dyn);
            }
        }

        public static bool Is(ICType expression) {
            return expression.IsTagged(Symbol.Lambda);
        }
    }
}