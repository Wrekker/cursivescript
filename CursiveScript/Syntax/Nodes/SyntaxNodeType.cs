namespace CursiveScript.Syntax.Nodes
{
    public enum SyntaxNodeType
    {
        None,
        Sequence,
        Literal,
        Splice,
        Expansion,
        Definition,
        Lambda,
        Condition,
        Macro,
        Assignment,
        SelfEvaluating,
        Variable,
        Application,
        While
    }
}