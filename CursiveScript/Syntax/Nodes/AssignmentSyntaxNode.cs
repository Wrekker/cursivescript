using System.Linq;
using CursiveScript.CTypes;

namespace CursiveScript.Syntax.Nodes
{
    public class AssignmentSyntaxNode : SyntaxNode
    {
        public Symbol Variable { get; private set; }
        public ICType Value { get; private set; }

        public AssignmentSyntaxNode(Pair expr) : base(expr, SyntaxNodeType.Assignment) {
            Variable = (Symbol)expr.Skip(1).First();
            Value = expr.Skip(2).First();
        }

        public static bool Is(ICType expression) {
            return expression.IsTagged(Symbol.Assignment);
        }
    }
}