﻿using CursiveScript.CTypes;

namespace CursiveScript.Syntax.Nodes
{
    public class LiteralSyntaxNode : SyntaxNode
    {
        public ICType Value { get; private set; }
        
        public LiteralSyntaxNode(Pair expr) : base(expr, SyntaxNodeType.Literal) {
            Value = ((Pair)expr.Cdr).Car;
        }

        public static bool Is(ICType expression) {
            return expression.IsTagged(Symbol.Literal);
        }
    }
}