using CursiveScript.CTypes;

namespace CursiveScript.Syntax.Nodes
{
    public class MacroSyntaxNode : SyntaxNode
    {
        public Symbol Variable { get; private set; }
        public ICType Parameters { get; private set; }
        public ICType Body { get; private set; }

        public MacroSyntaxNode(ICType expr) : base(expr, SyntaxNodeType.Macro) {
            var dyn = (expr as dynamic).Cdr;

            Variable = dyn.Car.Car as CTypes.Symbol;
            Parameters = dyn.Car.Cdr;

            dyn = dyn.Cdr;
            if (dyn.Cdr == Pair.Null && CTypeExtensions.IsTagged(dyn.Car, Symbol.Sequence)) {
                Body = dyn.Car;
            } else {
                Body =  SyntaxNode.CreateSequence(dyn);
            }
        }

        public static bool Is(ICType expression) {
            return expression.IsTagged(Symbol.Macro);
        }
    }
}