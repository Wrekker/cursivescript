using CursiveScript.CTypes;

namespace CursiveScript.Syntax.Nodes
{
    public class SpliceSyntaxNode : SyntaxNode
    {
        public ICType Value { get; private set; }

        public SpliceSyntaxNode(ICType expr) : base(expr, SyntaxNodeType.Splice) {
            Value = (expr as dynamic).Cdr.Car;
        }

        public static bool Is(ICType expression) {
            return expression.IsTagged(Symbol.Splice);
        }

        public override string ToString() {
            return "," + Value;
        }
    }
}