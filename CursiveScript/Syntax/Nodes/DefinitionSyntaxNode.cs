using System.Linq;
using CursiveScript.CTypes;

namespace CursiveScript.Syntax.Nodes
{
    public class DefinitionSyntaxNode : SyntaxNode
    {
        public Symbol Variable { get; private set; }
        public ICType Value { get; private set; }

        public DefinitionSyntaxNode(Pair expr) : base(expr, SyntaxNodeType.Definition) {
            var first = expr.Skip(1).First();

            if (first is Pair) {
                var flist = (Pair)first;

                Variable = (Symbol)flist.Car;
                Value =  SyntaxNode.CreateLambda(flist.Cdr,
                                           SyntaxNode.CreateSequence((expr as dynamic).Cdr.Cdr));
            } else {
                Variable = (Symbol)first;
                Value = expr.Skip(2).First();    
            }
            
        }

        public static bool Is(ICType expression) {
            return expression.IsTagged(Symbol.Definition);
        }
    }
}