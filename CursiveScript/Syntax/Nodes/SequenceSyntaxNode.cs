using CursiveScript.CTypes;

namespace CursiveScript.Syntax.Nodes
{
    public class SequenceSyntaxNode : SyntaxNode
    {
        public Pair Expressions { get; set; }

        public SequenceSyntaxNode(Pair expr) : base(expr, SyntaxNodeType.Sequence) {
            Expressions = (Pair)expr.Cdr;
        }

        public static bool Is(ICType expression) {
            return expression.IsTagged(Symbol.Sequence);
        }
    }
}