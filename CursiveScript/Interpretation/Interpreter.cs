﻿using System;
using System.Collections.Generic;
using System.Linq;
using CursiveScript.Analysis;
using CursiveScript.Analysis.ExecutionProcedures;
using CursiveScript.Analysis.ExecutionProcedures.Native;
using CursiveScript.Binding;
using CursiveScript.CTypes;
using CursiveScript.Interpretation.Binding;
using CursiveScript.Interpretation.Functions;
using CursiveScript.Primitives;
using CursiveScript.Runtime;
using CursiveScript.Runtime.Functions;
using CursiveScript.Runtime.Functions.Native;
using CursiveScript.Syntax.Nodes;
using Void = CursiveScript.CTypes.Void;

namespace CursiveScript.Interpretation
{
    public class Interpreter
    {
        private static readonly ICType EmptyProc = Void.Singleton;
        private static readonly ICType NullProc = Pair.Null;
        
        private readonly PatternBinderFactory _patternBinderFactory;

        public Interpreter(InterpreterEnvironment ipEnv) {
            _patternBinderFactory = new PatternBinderFactory();

            foreach (var nf in PrimitiveScript.NativeProcs) {
                ipEnv.Define(nf.Key).Value = nf.Value;
            }
        }

        public ICType Interpret(ICType expr, InterpreterEnvironment env) {
            if (expr.IsNull()) { return NullProc; }
            if (expr.IsEmpty()) { return EmptyProc; }

            switch (expr.Type) {
                case CType.Char:
                case CType.Real:
                    return InterpretSelfEvaluating(new SelfEvaluatingSyntaxNode(expr), env);
                case CType.Symbol:
                    return InterpretVariable((Symbol)expr, env);
                case CType.Pair:
                    var lst = (Pair)expr;

                    if (SequenceSyntaxNode.Is(expr)) { return InterpretSequence(new SequenceSyntaxNode(lst), env); }
                    if (AssignmentSyntaxNode.Is(expr)) { return InterpretAssignment(new AssignmentSyntaxNode(lst), env); }
                    if (DefinitionSyntaxNode.Is(expr)) { return InterpretDefinition(new DefinitionSyntaxNode(lst), env); }
                    if (MacroSyntaxNode.Is(expr)) { return InterpretMacroDefinition(new MacroSyntaxNode(lst), env); }
                    if (LambdaSyntaxNode.Is(expr)) { return InterpretLambda(new LambdaSyntaxNode(lst), env); }
                    if (ConditionSyntaxNode.Is(expr)) { return InterpretCondition(new ConditionSyntaxNode(lst), env); }
                    if (WhileSyntaxNode.Is(expr)) { return InterpretWhile(new WhileSyntaxNode(lst), env); }
                    if (LiteralSyntaxNode.Is(expr)) { return InterpretQuote(new LiteralSyntaxNode(lst), env); }

                    return InterpretApplication(new ApplicationSyntaxNode(lst), env);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private ICType InterpretWhile(WhileSyntaxNode expr, InterpreterEnvironment env) {
            ICType ret = Void.Singleton;
            while (Interpret(expr.Condition, env).IsTrue()) {
                ret = Interpret(expr.Body, env);
            }

            return ret;
        }

        public ICType InterpretCondition(ConditionSyntaxNode expr, InterpreterEnvironment env) {
            var p = Interpret(expr.Predicate, env);

            return p.IsTrue() ? 
                Interpret(expr.TrueBranch, env) : 
                Interpret(expr.FalseBranch, env);
        }

        private ICType InterpretMacroDefinition(MacroSyntaxNode expr, InterpreterEnvironment env) {
            IParameterBinder<InterpreterEnvironment> binder = _patternBinderFactory.Create(expr.Parameters);

            env.Define(expr.Variable).Value = new MacroInterpreterFunction(this, env, expr.Body, binder);
            return Void.Singleton;
        }

        public ICType InterpretLambda(LambdaSyntaxNode expr, InterpreterEnvironment env) {
            IParameterBinder<InterpreterEnvironment> binder = _patternBinderFactory.Create(expr.Parameters);
            
            return new LambdaInterpreterFunction(this, env, expr.Body, binder);
        }
        
        public ICType InterpretDefinition(DefinitionSyntaxNode expr, InterpreterEnvironment env) {
            var variable = expr.Variable;
            env.Define(variable).Value = Interpret(expr.Value, env);
            return Void.Singleton;
        }

        public ICType InterpretAssignment(AssignmentSyntaxNode expr, InterpreterEnvironment env) {
            var value = Interpret(expr.Value, env);
            env.Set(expr.Variable, value);

            return Void.Singleton;
        }

        public ICType InterpretSelfEvaluating(SelfEvaluatingSyntaxNode expr, InterpreterEnvironment env) {
            return expr.Value;
        }

        public ICType InterpretVariable(Symbol expr, InterpreterEnvironment env) {
            return env.Lookup(expr);
        }

        public ICType InterpretApplication(ApplicationSyntaxNode expr, InterpreterEnvironment env) {
            RunTimeFunction fn = (RunTimeFunction) Interpret(expr.Procedure, env);

            if (fn.IsType(CType.Macro)) {
                // This is slow as shit, but it works for now.
                // TODO: Turn this into a static rewrite
                return Interpret(fn.Apply(expr.Arguments), env);
            } else {
                var args = (Pair) expr.Arguments.Map(a => Interpret(a, env));
                return fn.Apply(args);
            }
        }
        
        public ICType InterpretSequence(SequenceSyntaxNode expr, InterpreterEnvironment env) {
            if (expr.Expressions == Pair.Null) {
                return EmptyProc;
            }

            ICType ret = Void.Singleton;
            foreach (var stmt in expr.Expressions) {
                ret = Interpret(stmt, env);
            }
            return ret;
        }

        public ICType InterpretQuote(LiteralSyntaxNode expr, InterpreterEnvironment env) {
            return InterpretLiteral(expr.Value, env);
        }

        public ICType InterpretLiteral(ICType expr, InterpreterEnvironment env) {
            if (expr.Equals(Pair.Null)) {
                return NullProc;
            }

            switch (expr.Type) {
                case CType.Symbol:                    
                case CType.Char:
                case CType.Real:
                    return InterpretSelfEvaluating(new SelfEvaluatingSyntaxNode(expr), env);
                case CType.Pair:
                    var lst = (Pair) expr;
                          
                    if (LiteralSyntaxNode.Is(lst)) {
                        return InterpretSelfEvaluating(new SelfEvaluatingSyntaxNode(((Pair)lst.Cdr).Car), env);
                    }

                    return InterpretLiteralList((Pair)expr, env);
            }
            
            throw new Exception();
        }
        
        public ICType InterpretLiteralExpansion(ExpansionSyntaxNode expr, InterpreterEnvironment env) {
            if (expr.Value == Pair.Null) { return NullProc; }

            return Interpret(expr.Value, env);
        }

        public ICType InterpretLiteralList(Pair lst, InterpreterEnvironment env) {
            if (lst == Pair.Null) { return NullProc; }

            ICType head = null;
            ICType tail;
            if (lst.Cdr.IsList()) {
                tail = InterpretLiteralList((Pair) lst.Cdr, env);
            } else {
                tail = InterpretLiteral(lst.Cdr, env);
            }

            if (SpliceSyntaxNode.Is(lst.Car)) {
                return InterpretLiteralSplice(new SpliceSyntaxNode(lst.Car), tail, env);
            } 

            if (ExpansionSyntaxNode.Is(lst.Car)) {
                head = InterpretLiteralExpansion(new ExpansionSyntaxNode(lst.Car), env);
            } else {
                head = InterpretLiteral(lst.Car, env);
            }

            return head.Cons(tail);
        }
        
        public ICType InterpretLiteralSplice(SpliceSyntaxNode expr, ICType tail, InterpreterEnvironment env) {
            var head = InterpretLiteralExpansion(new ExpansionSyntaxNode(expr.SourceSyntax), env);
            
            if (head.IsNull()) {
                return tail;
            }

            if (head.IsType(CType.Pair)) {
                return ((Pair)head).Append(tail);
            }

            return head.Cons(tail);
        }
    }
}