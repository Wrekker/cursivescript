﻿using CursiveScript.CTypes;

namespace CursiveScript.Interpretation
{
    public class InterpreterBinding
    {
        public Symbol Name { get; private set; }
        public ICType Value { get; set; }

        public InterpreterBinding(Symbol name, ICType value) {
            Name = name;
            Value = value;
        }

        public bool ResolvesAs(Symbol name) {
            return this.Name == name;
        }
    }
}