using CursiveScript.Binding;
using CursiveScript.CTypes;
using CursiveScript.Runtime.Functions;

namespace CursiveScript.Interpretation.Functions
{
    public class MacroInterpreterFunction : RunTimeFunction
    {
        private readonly Interpreter _ip;
        private readonly InterpreterEnvironment _env;
        private readonly ICType _body;
        private readonly IParameterBinder<InterpreterEnvironment> _binder;

        public override CType Type { get { return CType.Macro; } }

        public MacroInterpreterFunction(Interpreter ip, InterpreterEnvironment env, ICType body, IParameterBinder<InterpreterEnvironment> binder) {
            _ip = ip;
            _env = env;
            _body = body;
            _binder = binder;
        }

        public override ICType Apply(Pair args) {
            var env2 = _env.Extend();
            _binder.Bind(env2, args);

            return _ip.Interpret(_body, env2);
        }

        public override string ToString() {
            return "MacroFunction";
        }
    }
}