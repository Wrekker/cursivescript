using CursiveScript.Analysis;
using CursiveScript.Binding;
using CursiveScript.CTypes;

namespace CursiveScript.Interpretation.Binding
{
    public class PatternNullBinder : IParameterBinder<InterpreterEnvironment>
    {
        public void Bind(InterpreterEnvironment env, ICType arguments) { }
    }
}