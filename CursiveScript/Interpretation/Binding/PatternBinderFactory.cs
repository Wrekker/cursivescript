﻿using CursiveScript.Analysis;
using CursiveScript.Binding;
using CursiveScript.CTypes;

namespace CursiveScript.Interpretation.Binding
{
    public class PatternBinderFactory : IParameterBinderFactory<InterpreterEnvironment>
    {
        public IParameterBinder<InterpreterEnvironment> Create(ICType expr) {
            if (expr.IsNull()) {
                return new PatternNullBinder();
            }

            if (expr.IsType(CType.Symbol)) {
                return new PatternSymbolBinder((Symbol)expr);
            }

            return new PatternPairBinder(
                Create(((Pair) expr).Car), 
                Create(((Pair) expr).Cdr));
        }

    }
}