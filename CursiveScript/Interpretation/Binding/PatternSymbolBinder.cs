using CursiveScript.Analysis;
using CursiveScript.Binding;
using CursiveScript.CTypes;

namespace CursiveScript.Interpretation.Binding
{
    public class PatternSymbolBinder : IParameterBinder<InterpreterEnvironment>
    {
        private readonly Symbol _name;

        public PatternSymbolBinder(Symbol name) {
            _name = name;
        }

        public void Bind(InterpreterEnvironment env, ICType arguments) {
            env.Define(_name).Value = arguments;
        }
    }
}