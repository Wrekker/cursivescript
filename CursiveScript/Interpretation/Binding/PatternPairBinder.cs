using CursiveScript.Analysis;
using CursiveScript.Binding;
using CursiveScript.CTypes;

namespace CursiveScript.Interpretation.Binding
{
    public class PatternPairBinder : IParameterBinder<InterpreterEnvironment>
    {
        public IParameterBinder<InterpreterEnvironment> CarBinder { get; set; }
        public IParameterBinder<InterpreterEnvironment> CdrBinder { get; set; }

        public PatternPairBinder(IParameterBinder<InterpreterEnvironment> car, IParameterBinder<InterpreterEnvironment> cdr) {
            CarBinder = car;
            CdrBinder = cdr;
        }

        public void Bind(InterpreterEnvironment env, ICType arguments) {
            if (arguments.IsEmpty()) {
                arguments = Void.Singleton.Cons(Void.Singleton);
            }

            CarBinder.Bind(env, ((Pair) arguments).Car);
            CdrBinder.Bind(env, ((Pair) arguments).Cdr);
        }
    }
}