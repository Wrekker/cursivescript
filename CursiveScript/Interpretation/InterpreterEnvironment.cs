﻿using System;
using System.Collections.Generic;
using System.Linq;
using CursiveScript.CTypes;

namespace CursiveScript.Interpretation
{
    public class InterpreterEnvironment
    {
        private readonly Tuple<List<InterpreterBinding>, object> _frames;
        
        private int _depth = 0;

        public int Count { get { return _frames.Item1.Count; } }

        public InterpreterEnvironment() {
            _frames = new Tuple<List<InterpreterBinding>, object>(new List<InterpreterBinding>(), null);
        }

        private InterpreterEnvironment(InterpreterEnvironment enclosing) {
            _frames = new Tuple<List<InterpreterBinding>, object>(new List<InterpreterBinding>(), enclosing._frames);
            _depth = enclosing._depth + 1;
        }

        private InterpreterBinding Resolve(Symbol name) {
            var f = _frames;
            
            while (f != null) {
                var bindings = f.Item1;
                for (int i = bindings.Count - 1; i >= 0; i--) {
                    if (bindings[i].ResolvesAs(name)) {
                        return bindings[i];
                    }
                }

                f = (Tuple<List<InterpreterBinding>, object>)f.Item2;
            }

            throw new Exception("Reference to unbound variable: " + name);
        }

        public InterpreterBinding Define(Symbol name) {
            if (_frames.Item1.Any(a => a.ResolvesAs(name))) {
                throw new Exception(name + " is already defined.");
            }
            
            var ret = new InterpreterBinding(name, null);

            _frames.Item1.Add(ret);

            return ret;
        }

        public ICType Lookup(Symbol name) {
            InterpreterBinding binding = Resolve(name);
            return binding.Value;
        }

        public ICType[] Lookup(int count) {
            var ret = new List<ICType>();
            
            var bindings = _frames.Item1;
            for (int i = bindings.Count - count; i < bindings.Count; i++) {
                ret.Add(bindings[i].Value);
            }

            return ret.ToArray();
        }

        public InterpreterEnvironment Extend() { return Extend(null, null); }
        public InterpreterEnvironment Extend(IEnumerable<ICType> names, IEnumerable<ICType> parameters) {
            var env = new InterpreterEnvironment(this);

            if (names == null || parameters == null) {
                return env;
            }

            var nameEnum = names.GetEnumerator();
            var paramEnum = parameters.GetEnumerator();

            while (nameEnum.MoveNext() && paramEnum.MoveNext()) {
                var binding = env.Define((Symbol) nameEnum.Current);
                binding.Value = paramEnum.Current;
            }
            
            return env;
        }

        public void Set(Symbol variable, ICType value) {
            var binding = Resolve(variable);
            binding.Value = value;
        }
    }
}