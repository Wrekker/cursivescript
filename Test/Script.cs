using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using CursiveScript.Analysis;
using CursiveScript.Analysis.ExecutionProcedures;
using CursiveScript.CTypes;
using CursiveScript.Macros;
using CursiveScript.Runtime;
using CursiveScript.Syntax.Parsing;
using CursiveScript.Transformation;

namespace Test
{
    public class Script
    {
        private RunTimeEnvironment _baseEnv;
        private AnalysisEnvironment _baseCEnv;
        private Analyzer _analyzer;
        //private MacroExpander _macroExpander = new MacroExpander();
        //private DefinitionRearranger _definitionRearranger = new DefinitionRearranger();
        private Importer _importer = new Importer();
        
        public delegate Dictionary<string, decimal?> ScriptExecutor(IEnumerable<decimal?> values);

        public Script(string headerScript) {
            var cEnv = new AnalysisEnvironment();
            var env = new RunTimeEnvironment();
            
            Analyzer analyzer = new Analyzer(cEnv, env);

            Tokenizer tokenizer = new Tokenizer(headerScript);
            Parser parser = new Parser();
            
            var ast = parser.Parse(tokenizer);
            ast = _importer.RewriteTree(ast);
            analyzer.Analyze(ast, cEnv).Call(env);

            _analyzer = analyzer;
            _baseCEnv = cEnv;
            _baseEnv = env;
        }

        public ScriptExecutor MakeExecutor(IEnumerable<string> variables, IDictionary<string, string> indices) {
            variables = variables ?? Enumerable.Empty<string>();

            var cEnv2 = _baseCEnv.Extend(variables.Select(Symbol.Create));
            cEnv2 = cEnv2.Extend();

            var sbIndices = new StringBuilder("(begin \n");
            var sbExport = new StringBuilder("(list");
            foreach (var index in indices) {
                sbIndices.AppendFormat("(define &{0} (lambda () {1}))\n", index.Key, string.IsNullOrWhiteSpace(index.Value) ? "null" : index.Value);
                sbExport.AppendFormat(" (&{0})", index.Key);
            }
            sbExport.Append(")");
            sbIndices.Append(")");

            var parser = new Parser();
            var tokenizer = new Tokenizer(sbIndices.ToString());

            var ast = parser.Parse(tokenizer);
            //ast = _macroExpander.RewriteTree(ast);
            //ast = _definitionRearranger.RewriteTree(ast);
            
            ExecProc indexProc = _analyzer.Analyze(ast, cEnv2);
            cEnv2 = cEnv2.Extend();

            parser = new Parser();
            tokenizer = new Tokenizer(sbExport.ToString());
            ast = parser.Parse(tokenizer);

            ExecProc exportProc = _analyzer.Analyze(ast, cEnv2);

            return (values) => {
                var env2 = _baseEnv.Extend(values.Select(s => s == null ? null : new Fraction(s.Value)));
                env2 = env2.Extend();
                indexProc.Call(env2); // Ignore return value

                env2 = env2.Extend();
                Pair res = (Pair)exportProc.Call(env2);

                var ret = new Dictionary<string, decimal?>();
                var ne = indices.Keys.GetEnumerator();
                var ve = res.GetEnumerator();

                while (ne.MoveNext() && ve.MoveNext()) {
                    ret.Add(ne.Current, (decimal?)(Fraction)(ve.Current.IsNull() ? null : ve.Current));
                }

                return ret;
            };
        }
    }
}