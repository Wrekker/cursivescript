﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web.Script.Serialization;
using CursiveScript.Addressing;
using CursiveScript.Analysis;
using CursiveScript.CTypes;
using CursiveScript.Interpretation;
using CursiveScript.Macros;
using CursiveScript.Primitives;
using CursiveScript.Runtime;
using CursiveScript.Syntax.Parsing;
using CursiveScript.Transformation;

namespace Test
{
    class Program
    {
        private static string tst = @"(define (map fn (f . r))
	(if (empty? f)
		null
		(cons (fn f) (map fn r))))

(define (length (f . r))
	(if (empty? f)
		0
		(+ 1 (length r))))

(define-macro (cond (pred . exprs) . rest)		
	(if (empty? pred)
		pred
		(if (eq? pred 'else)
			(if (cdr exprs)
				'(begin ,@exprs)
				(car exprs))
			'(if ,pred
				,(if (cdr exprs)
					'(begin ,@exprs)
					(car exprs))
				(cond ,@rest)))))
        
(define-macro (let bindings . exprs)
	'((lambda (,@(map car bindings))
		,@exprs) 
    ,@(map (lambda (x) (car (cdr x))) bindings)))
				
(define-macro (and f . r)
	'(if (not ,f)
			#f
			,(if (null? r)
				#t
				'(and ,@r))))

(define-macro (or f . r)
	'(if ,f
			#t
			,(if (null? r)
				#f
				'(or ,@r))))
				
(define-macro (?? var default)
	'(if (empty? ,var)
			(set! ,var ,default))) (define (count (f . r) pred)
  (cond
    ((empty? f)
      0)
    ((pred f)
      (+ 1 (count r pred)))
    (else
      (count r pred))))

(define (sum (f . r))
  (if (empty? f)
    0
    (+ f (sum r))))
    
(define (mod a b)
  (if (> a b)
    (mod (- a b) b)
    (if (= b a) 0 a)))

(define (bit-set? b num)
  (cond
    ((= b 0)
      (= (mod num 2) 1))
    (else
      (bit-set? (- b 1) (/ num 2)))))

(define (filter (f . r) pred)
  (cond 
    ((empty? f) null)
    ((pred f) (filter r pred))
    (else (cons f (filter r pred)))))
    
(define (member? (h . r) it)
  (cond
    ((empty? h) #f)
    ((= h it) #t)
    (else (member? r it))))

(define (mean min . terms)
    (if (> min (- (length terms) (count terms null?)))
      null
      (/ 	(sum (filter terms null?)) 
        (length (filter terms null?)))))	

(define (varcount min . terms)
    (if (> min (- (length terms) (count terms null?)))
      null				
      (length (filter terms null?))))

(define (countval val (f . r))
  (cond
    ((empty? f) 0)
    ((= f val) (+ 1 (countval val r)))
    (else (countval val r))))

(define (varpercent x val)
  (?? val 4)
  (* (/ (- x 1) val) 100))
 
(define (percent x val)
  (?? val 1)
  (* (/ x val) 100))

(define (isnull x val)
  (if (= x null)
    val
    x))	

(define-macro (apply fn args)
  (cons fn args))

(define (avg . vals)
  (/ (sum vals) (length vals)))

(define-macro (recode var (case ret) . rest)
  (if (empty? case)
    null	
    (if (pair? case)
      '(if (and (>= ,var ,(car case)) (<= ,var ,(cdr case)))
        ,ret
        (recode ,var ,@rest))
      (if (eq? case 'else)
        ret
        '(if (= ,var ,case)
          ,ret
          (recode ,var ,@rest))))))
";

        static BigInteger Fac(BigInteger n) {
            if (n < 3) return n;
            BigInteger q = Fac(n - 1);
            return n*q;
        }

        static void Main(string[] args) {
            var codeBody = File.ReadAllText(args.Length > 0 ? args[0] : "testscript.cst");

            //var packageCompiler = new DefaultPackageCompiler();
            //packageCompiler.Compile(new IPackage [] { new BasePackage() }, ref cEnv, ref env, ref codeBody);
            
            Stopwatch sw;
            sw = Stopwatch.StartNew();
            Tokenizer tokenizer = new Tokenizer(codeBody);
            Parser parser = new Parser();

            var ast = parser.Parse(tokenizer);

            var importer = new Importer();
            //var rearranger = new DefinitionRearranger();
            //var expander = new MacroExpander();

            ast = importer.RewriteTree(ast);
            //ast = expander.RewriteTree(ast);
            //ast = rearranger.RewriteTree(ast);

            //sw = Stopwatch.StartNew();
            //BigInteger q = 0;
            //for (int i = 0; i < 1000; i++) {
            //    q = Fac(500);
            //}
            //Console.WriteLine(q);
            //Console.WriteLine("{0}", sw.ElapsedMilliseconds);

            StdIo.Out = Console.Out;
            Console.WriteLine("Parsing:  {0}", sw.ElapsedMilliseconds);

            //Console.WriteLine("Interpreter:");
            //sw = Stopwatch.StartNew();
            //TestInterpreter(ast);
            //Console.WriteLine("  {0}", sw.ElapsedMilliseconds);

            Console.WriteLine("Analyzer:");
            sw = Stopwatch.StartNew();
            TestExecProcs(ast);
            Console.WriteLine("  {0}", sw.ElapsedMilliseconds);

            //Console.WriteLine("Script:");
            //sw = Stopwatch.StartNew();
            //TestScript();
            //Console.WriteLine(sw.ElapsedMilliseconds);

            Console.Read();
        }

        private static void TestExecProcs(ICType ast) {
            var cEnv = new AnalysisEnvironment();
            var env = new RunTimeEnvironment();

            Analyzer ip = new Analyzer(cEnv, env);
            var ep = ip.Analyze(ast, cEnv);
            
            Stopwatch sw = Stopwatch.StartNew();
            ep.Call(env);
            Console.WriteLine("   Execution: {0}", sw.ElapsedMilliseconds);
        }

        private static void TestInterpreter(ICType ast) {
            InterpreterEnvironment env = new InterpreterEnvironment();
            Interpreter ip = new Interpreter(env);
            
            Stopwatch sw = Stopwatch.StartNew();
            ICType result = ip.Interpret(ast, env);
            Console.WriteLine("   Execution: {0}", sw.ElapsedMilliseconds);
        }


        static void TestScript() {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            //var data = new Dictionary<string, Dictionary<string, decimal?>>();

            var data = serializer.Deserialize<Dictionary<string, Dictionary<string, decimal?>>>(File.ReadAllText(@"G:\Users\tse\Desktop\data.txt"));

            //for (int i = 0; i < 1; i++) {
            //    var resp = new Dictionary<string, decimal?> {{"a", 1}, {"b", 2}, {"c", 3}};
            //    for (int j = 0; j < 200; j++) {
            //        resp.Add(j.ToString(), j);
            //    }

            //    data.Add(i.ToString(), resp);
            //}

            var indicies = serializer.Deserialize<Dictionary<string, string>>(File.ReadAllText("../../snippets.txt")); // new Dictionary<string, string>();

            //for (int i = 0; i < 200; i+=3) {
            //    indicies.Add("idx" + i, "(recode a (1 1) (else 0))");
            //    indicies.Add("idx" + (i+1), "(recode b (1 1) (else 0))");
            //    indicies.Add("idx" + (i+2), "(recode c (1 1) (else 0))");
            //};

            //var header = PrimitiveScript.Text + "\n" + File.ReadAllText("../../scheme_include.cst");

            var vars = data.First().Value.Keys.Select(s => s);

            Script scr = new Script(tst);
            var exec = scr.MakeExecutor(vars, indicies);

            Stopwatch sw = Stopwatch.StartNew();
            foreach (var resp in data) {
                
                var result = exec(resp.Value.Select(s => s.Value));

                foreach (KeyValuePair<string, decimal?> kv in result) {
                    //Console.WriteLine("{0}: {1}", kv.Key, kv.Value);
                }
            }
            Console.WriteLine("   Inner: " + sw.ElapsedMilliseconds);
            
        }
        
    }
}
